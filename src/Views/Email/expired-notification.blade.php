<html>
    <head>
        <style>
            table{
                border-collapse: collapse;
                margin : auto;
            }
            td{
                padding : 10px;
            }
            th{
                /* text-align : center; */
                padding : 10px;
            }
            tr{
                border-bottom : 1px solid #ccc;
            }
            div{
                width : 100%;
            }
            h4{
                text-align : center;
            }
            h3{
                text-align : center;
                padding : 30px;
            }
        </style>
    </head>
<body>
<p>Dear <i>Admin</i></p>

<p>Message description</p>

<h3>Stocks Notifications</h3>


@if ( ! empty($stockList) )

    <h4>Expired Stocks</h4>

    <table>
        <tr>
            <th>Name</th>
            <th>Total Quantity</th>
            <th>Expiration Date</th>
            <th>Location</th>
        </tr>
        @foreach ($stockList as $item)
        <tr>
            <td>{{ $item->item->name }}</td>
            <td>{{ $item->quantity }}</td>
            <td>{{ date('F j, Y', strtotime($item->expiration_date)) }}</td>
            <td>{{ $item->location->name }}</td>
        </tr>
        @endforeach
    </table>

@endif

<p><i>THIS IS AN AUTOMATED MESSAGE - PLEASE DO NOT REPLY DIRECTLY TO THIS EMAIL</i></p>

</body>
</html>
