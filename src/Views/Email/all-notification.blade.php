<html>
    <head>
        <style>
            table{
                border-collapse: collapse;
                /* margin : auto; */
            }
            td, th {
                padding : 10px;
                border : 1px dashed #ccc;
            }
            div{
                width : 100%;
                border-bottom : 1px dashed #ccc;
                padding-bottom : 30px;
                margin-bottom : 30px;
            }
            h4{
                /* text-align : center; */
            }
            h3{
                /* text-align : center; */
                /* padding-bottom : 30px; */
            }
        </style>
    </head>
<body>
<p>Dear <i>Admin</i></p>

<p>Message description</p>

<h3>Stocks Notifications</h3>


@if ( ! empty($thresholdList) )
<div>
    <h4>Stock below Threshold</h4>

    <table>
        <tr>
            <th>Name</th>
            <th>Total Quantity</th>
            <th>Threshold</th>
            <th>Location</th>
        </tr>
        @foreach ($thresholdList as $item)
        <tr>
            <td>{{ $item->stock->name }}</td>
            <td>{{ $item->qty_total }}</td>
            <td>{{ $item->qty_threshold }}</td>
            <td>{{ $item->location->name }}</td>
        </tr>
        @endforeach
    </table>
</div>
@endif

@if ( ! empty($expiredList) )
<div>
    <h4>Expired Stocks</h4>

    <table>
        <tr>
            <th>Name</th>
            <th>Total Quantity</th>
            <th>Expiration Date</th>
            <th>Location</th>
        </tr>
        @foreach ($expiredList as $item)
        <tr>
            <td>{{ $item->item->name }}</td>
            <td>{{ $item->quantity }}</td>
            <td>{{ date('F j, Y', strtotime($item->expiration_date)) }}</td>
            <td>{{ $item->location->name }}</td>
        </tr>
        @endforeach
    </table>
</div>
@endif

@if ( ! empty($nearExpiryList) )
<div>
    <h4>Near Expiry Stocks</h4>

    <table>
        <tr>
            <th>Name</th>
            <th>Total Quantity</th>
            <th>Expiration Date</th>
            <th>Location</th>
        </tr>
        @foreach ($nearExpiryList as $item)
        <tr>
            <td>{{ $item->item->name }}</td>
            <td>{{ $item->quantity }}</td>
            <td>{{ date('F j, Y', strtotime($item->expiration_date)) }}</td>
            <td>{{ $item->location->name }}</td>
        </tr>
        @endforeach
    </table>
</div>
@endif

<p><i>THIS IS AN AUTOMATED MESSAGE - PLEASE DO NOT REPLY DIRECTLY TO THIS EMAIL</i></p>

</body>
</html>
