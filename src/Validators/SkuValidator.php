<?php

namespace Sunnydevbox\TWInventory\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class SkuValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'inventory_id'  	=> 'required',
            'code'              => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'inventory_id'      => 'required',
            'code'              => 'required',
        ]
   ];

}