<?php

namespace Sunnydevbox\TWInventory\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class PurchaseOrderItemValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'quantity'              => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'quantity'              => 'required',
        ],
        'attached_item' => [
            'price' => 'required',
            'stock_id' => 'required',
        ],
   ];

}
