<?php

namespace Sunnydevbox\TWInventory\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class PurchaseOrderValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'supplier_id'              => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'supplier_id'              => 'required',
        ]
   ];

}