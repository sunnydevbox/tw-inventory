<?php

namespace Sunnydevbox\TWInventory\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class SupplierValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name'  	=> 'required|min:3',
            'address'  	=> 'required',
            //'postal_code'   => 'required',
            'zip_code'   => 'required',
            'region'   => 'required',
            'city'   => 'required',
            'country'   => 'required',
            //'contact_title'   => 'required',
            'contact_name'   => 'required',
            'contact_phone'   => 'required',
            //'contact_fax'   => 'required',
            'contact_email'   => 'required|email',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name'      => 'required|min:3',
            'address'   => 'required',
            //'postal_code'   => 'required',
            'zip_code'   => 'required',
            'region'   => 'required',
            'city'   => 'required',
            'country'   => 'required',
            //'contact_title'   => 'required',
            'contact_name'   => 'required',
            'contact_phone'   => 'required',
            //'contact_fax'   => 'required',
            'contact_email'   => 'required|email',
        ]
   ];

}