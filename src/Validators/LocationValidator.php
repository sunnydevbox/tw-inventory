<?php

namespace Sunnydevbox\TWInventory\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class LocationValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name'  	=> 'required|min:3',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name'      => 'required|min:3',
        ]
   ];

}