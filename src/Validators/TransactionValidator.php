<?php

namespace Sunnydevbox\TWInventory\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class TransactionValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'stock_id'  => 'required',
            'state'     => 'required',
            'quantity'  => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'stock_id'  => 'required',
            'state'     => 'required',
            'quantity'  => 'required',
        ]
   ];

}