<?php

namespace Sunnydevbox\TWInventory\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class SaleOrderValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'customer_id'              => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'customer_id'              => 'required',
        ]
   ];

}