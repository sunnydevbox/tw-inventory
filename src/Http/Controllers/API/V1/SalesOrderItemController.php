<?php

namespace Sunnydevbox\TWInventory\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;
use \Prettus\Validator\Exceptions\ValidatorException;
use \Prettus\Validator\Contracts\ValidatorInterface;

class SalesOrderItemController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWInventory\Repositories\SaleOrder\SaleOrderItemRepository $repository,
		\Sunnydevbox\TWInventory\Transformers\SaleOrderItemTransformer $transformer,
		\Sunnydevbox\TWInventory\Services\SaleOrderService $service,
		\Sunnydevbox\TWInventory\Services\TransactionService $transactionService,
		\Sunnydevbox\TWInventory\Services\SaleOrderItemService $saleOrderItemService
	) {
		$this->transformer = $transformer;
		$this->repository = $repository; 
		$this->service = $service;
		$this->saleOrderItemService = $saleOrderItemService;
		$this->transactionService = $transactionService;
	}

	public function store(Request $request)
	{
		try {
			
			if (isset($this->validator) && $this->validator) {
				   $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
			}

			$result = $this->service->attachItem($request->all());
			// $result = $this->show($result->id);
            return $this->response->item($result, $this->transformer);

        } catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
        }
	}

	public function update(Request $request, $id)
	{
		try {
			if (isset($this->validator) && $this->validator) {
				   $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
			}
           
			$result = $this->service->updateItem($request->all(), $id);

			return $result;

        } catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
        }
	}

	public function destroy($id)
	{
		$obj = $this->repository->find($id);
		
		if (!$obj && isset($this->return_messages)) {
			return $this->response->error($this->return_messages['object_not_found'], 404);
		}
		// dd($obj);
		if ($this->service->removeItem($obj)) {
			// $this->transactionService->releaseReserved($obj->inventory_transaction_id);
			return $this->response->noContent();
		} else {
			return $this->response->error('could_not_delete_book', 500);
		}
	}

	public function serve($action, $soiId, Request $request)
	{ 
		/**
		 * for release serve the $poiId is the transaction ID
		 */
		switch($action){
			case 'add-box-id' : 
				
				$result = $this->saleOrderItemService->addBox($soiId, $request);
				// $result = $this->response->collection($result, $this->transformer);
			break;
		}
		
		return $result;
	}

}