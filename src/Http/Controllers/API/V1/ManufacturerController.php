<?php

namespace Sunnydevbox\TWInventory\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class ManufacturerController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWInventory\Repositories\Manufacturer\ManufacturerRepository $repository,
		\Sunnydevbox\TWInventory\Transformers\ManufacturerTransformer $transformer,
		\Sunnydevbox\TWInventory\Validators\ManufacturerValidator $validator
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
	}

}