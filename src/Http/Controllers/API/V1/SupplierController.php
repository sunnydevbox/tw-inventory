<?php

namespace Sunnydevbox\TWInventory\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class SupplierController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWInventory\Repositories\Supplier\SupplierRepository $repository,
		\Sunnydevbox\TWInventory\Transformers\SupplierTransformer $transformer,
		\Sunnydevbox\TWInventory\Validators\SupplierValidator $validator
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
	}

}