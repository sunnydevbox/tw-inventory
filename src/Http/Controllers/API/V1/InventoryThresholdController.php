<?php

namespace Sunnydevbox\TWInventory\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class InventoryThresholdController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWInventory\Repositories\InventoryThreshold\InventoryThresholdRepository $repository,
		\Sunnydevbox\TWInventory\Transformers\InventoryThresholdTransformer $transformer,
		\Sunnydevbox\TWInventory\Validators\InventoryThresholdValidator $validator,
		\Sunnydevbox\TWInventory\Services\InventoryNotificationService $service
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
		$this->service = $service;
	}

	public function getNotifications()
	{
		$this->service->sendAllNotificaton();

		return $this->repository->get();
	}

	

}