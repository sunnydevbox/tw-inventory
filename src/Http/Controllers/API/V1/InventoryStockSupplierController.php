<?php

namespace Sunnydevbox\TWInventory\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use \Prettus\Validator\Exceptions\ValidatorException;
use \Prettus\Validator\Contracts\ValidatorInterface;
use Dingo\Api\Http\Request;

class InventoryStockSupplierController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWInventory\Repositories\Stock\StockMovementRepository $repository,
		\Sunnydevbox\TWInventory\Transformers\StockTransformer $transformer,
		\Sunnydevbox\TWInventory\Validators\StockValidator $validator,
		\Sunnydevbox\TWInventory\Services\StockManagementService $service
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
		$this->service = $service;
	}

}
