<?php

namespace Sunnydevbox\TWInventory\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use \Prettus\Validator\Exceptions\ValidatorException;
use \Prettus\Validator\Contracts\ValidatorInterface;
use Dingo\Api\Http\Request;

class InventoryStockController extends APIBaseController
{

	public function store(Request $request)
	{
		try {
			$this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

			$result = $this->service->store($request);
			
			return $this->response->item($result, $this->transformer);

		 } catch (ValidatorException $e) {

			 return response()->json([
				 'status_code'   => 400,
				 'message' =>$e->getMessageBag()
			 ], 400);
		 }
	}

	public function __construct(
		\Sunnydevbox\TWInventory\Repositories\Stock\StockRepository $repository,
		\Sunnydevbox\TWInventory\Transformers\StockTransformer $transformer,
		\Sunnydevbox\TWInventory\Validators\StockValidator $validator,
		\Sunnydevbox\TWInventory\Services\StockManagementService $service
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
		$this->service = $service;
	}

}
