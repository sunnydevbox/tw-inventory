<?php

namespace Sunnydevbox\TWInventory\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use \Prettus\Validator\Exceptions\ValidatorException;
use \Prettus\Validator\Contracts\ValidatorInterface;
use Dingo\Api\Http\Request;

class InventoryTransactionController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWInventory\Repositories\Transaction\TransactionRepository $repository,
		\Sunnydevbox\TWInventory\Transformers\TransactionTransformer $transformer,
		\Sunnydevbox\TWInventory\Validators\TransactionValidator $validator,
		\Sunnydevbox\TWInventory\Services\TransactionService $service
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
		$this->service  = $service;
	}


	public function store(Request $request)
	{
		$this->service->store($request);
	}

	public function releaseServe($id)
	{		
		return $this->service->releaseServe($id);
	} 

}