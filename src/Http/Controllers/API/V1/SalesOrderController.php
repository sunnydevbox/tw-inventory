<?php

namespace Sunnydevbox\TWInventory\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;

use Sunnydevbox\TWInventory\Events\SaleOrderTransactionEvent;

class SalesOrderController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWInventory\Repositories\SaleOrder\SaleOrderRepository $repository,
		\Sunnydevbox\TWInventory\Transformers\SaleOrderTransformer $transformer,
		\Sunnydevbox\TWInventory\Services\SaleOrderService $service
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->service = $service;
	}

	public function store(Request $request)
	{
		try {
			
			if (isset($this->validator) && $this->validator) {
				   $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
			}
			// dd(1);
			$result = $this->service->store($request->all());
			// dd(1);
			// $result = $this->show($result->id);
            return $this->show($result->id);

        } catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
        }
	}

	public function processOrder($action, $id)
	{
		if (is_null($id) || is_null($action)) {
			return response()->json([
                'status_code'   => 400,
                'message' => 'invalid_id_or_action',
            ], 400);
		}

		$result = $this->service->processOrder($action, $id);
		// dd('trigger vent');
		event(new SaleOrderTransactionEvent('so_status_' . $action, $result));

		return $this->response->item($result, $this->transformer);
	}

	public function update(Request $request, $id)
	{
		try {
			if (isset($this->validator) && $this->validator) {
				   $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
			}

			$result = $this->service->update($request->all(), $id);

			return $this->response->noContent();

        } catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
        }
	}
}