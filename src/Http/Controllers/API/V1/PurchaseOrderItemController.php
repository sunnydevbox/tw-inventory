<?php

namespace Sunnydevbox\TWInventory\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;
use \Prettus\Validator\Exceptions\ValidatorException;
use \Prettus\Validator\Contracts\ValidatorInterface;

class PurchaseOrderItemController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWInventory\Repositories\PurchaseOrder\PurchaseOrderItemRepository $repository,
		\Sunnydevbox\TWInventory\Transformers\PurchaseOrderItemTransformer $transformer,
		\Sunnydevbox\TWInventory\Services\PurchaseOrderService $service,
		\Sunnydevbox\TWInventory\Services\PurchaseOrderItemService $purchaseOrderItemService
	) {
		$this->transformer = $transformer;
		$this->repository = $repository; 
		$this->service = $service;
		$this->purchaseOrderItemService = $purchaseOrderItemService;
	}

	public function store(Request $request)
	{
		try {
			
			if (isset($this->validator) && $this->validator) {
				   $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
			}
			
			$result = $this->service->attachItem($request->all());
			// $result = $this->show($result->id);
            return $this->response->item($result, $this->transformer);

        } catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
        }
	}

	public function update(Request $request, $id)
	{
		try {
			if (isset($this->validator) && $this->validator) {
				   $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
			}
           
			$result = $this->service->updateItem($request->all(), $id);

			return $this->response->noContent();

        } catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
        }
	}

	public function destroy($id)
	{
		$obj = $this->repository->find($id);
		
		if (!$obj && isset($this->return_messages)) {
			return $this->response->error($this->return_messages['object_not_found'], 404);
		}

		if ($this->service->removeItem($obj)) {
			return $this->response->noContent();
		} else {
			return $this->response->error('could_not_delete_book', 500);
		}
	}


	public function serve($action, $poiId, Request $request)
	{
		/**
		 * for release serve the $poiId is the transaction ID
		 */
		switch($action){
			case 'serve' : 
				$result = $this->purchaseOrderItemService->serve($action, $poiId, $request);
				$result = $this->response->collection($result, $this->transformer);
			break;
			case 'release-serve' : 
				$result = $this->purchaseOrderItemService->releaseServe($poiId);
			break;
		}
		
		return $result;
	}

}