<?php

namespace Sunnydevbox\TWInventory\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class InventorySkuController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWInventory\Repositories\Sku\SkuRepository $repository,
		\Sunnydevbox\TWInventory\Transformers\SkuTransformer $transformer,
		\Sunnydevbox\TWInventory\Validators\SkuValidator $validator
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
	}

}