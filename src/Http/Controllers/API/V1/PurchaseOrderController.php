<?php

namespace Sunnydevbox\TWInventory\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;

use Sunnydevbox\TWInventory\Events\PurchaseOrderTransactionEvent;

class PurchaseOrderController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWInventory\Repositories\PurchaseOrder\PurchaseOrderRepository $repository,
		\Sunnydevbox\TWInventory\Transformers\PurchaseOrderTransformer $transformer,
		\Sunnydevbox\TWInventory\Services\PurchaseOrderService $service
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->service = $service;
	}

	public function store(Request $request)
	{
		try {
			
			if (isset($this->validator) && $this->validator) {
				   $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
			}

			$result = $this->service->store($request->all());
			
			return $this->show($result->id);

        } catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
        }
	}

	public function update(Request $request, $id)
	{
		try {
			if (isset($this->validator) && $this->validator) {
				   $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
			}

			$result = $this->service->update($request->all(), $id);

			return $this->response->noContent();

        } catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
        }
	}

	public function processOrder($action, $id)
	{
		if (is_null($id) || is_null($action)) {
			return response()->json([
                'status_code'   => 400,
                'message' => 'invalid_id_or_action',
            ], 400);
		}

		$result = $this->service->processOrder($action, $id);

		event(new PurchaseOrderTransactionEvent('po_status_' . $action, $result));

		return $this->response->item($result, $this->transformer);
	}
}