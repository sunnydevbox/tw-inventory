<?php

namespace Sunnydevbox\TWInventory\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class CustomerController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWInventory\Repositories\Customer\CustomerRepository $repository,
		\Sunnydevbox\TWInventory\Transformers\CustomerTransformer $transformer,
		\Sunnydevbox\TWInventory\Validators\CustomerValidator $validator
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
	}

}