<?php

namespace Sunnydevbox\TWInventory\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class DeliveryMethodController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWInventory\Repositories\DeliveryMethod\DeliveryMethodRepository $repository,
		\Sunnydevbox\TWInventory\Transformers\DeliveryMethodTransformer $transformer,
		\Sunnydevbox\TWInventory\Validators\DeliveryMethodValidator $validator
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
	}

}