<?php

namespace Sunnydevbox\TWInventory\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class MetricController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWInventory\Repositories\Metric\MetricRepository $repository,
		\Sunnydevbox\TWInventory\Transformers\MetricTransformer $transformer,
		\Sunnydevbox\TWInventory\Validators\MetricValidator $validator
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
	}

}