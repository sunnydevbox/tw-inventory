<?php

namespace Sunnydevbox\TWInventory\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class PaymentTermController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWInventory\Repositories\PaymentTerm\PaymentTermRepository $repository,
		\Sunnydevbox\TWInventory\Transformers\PaymentTermTransformer $transformer,
		\Sunnydevbox\TWInventory\Validators\PaymentTermValidator $validator
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
	}

}