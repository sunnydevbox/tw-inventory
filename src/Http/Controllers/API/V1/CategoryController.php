<?php

namespace Sunnydevbox\TWInventory\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class CategoryController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWInventory\Repositories\Category\CategoryRepository $repository,
		\Sunnydevbox\TWInventory\Transformers\CategoryTransformer $transformer,
		\Sunnydevbox\TWInventory\Validators\CategoryValidator $validator
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
	}

}