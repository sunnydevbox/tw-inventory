<?php

namespace Sunnydevbox\TWInventory\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use \Prettus\Validator\Exceptions\ValidatorException;
use \Prettus\Validator\Contracts\ValidatorInterface;
use Dingo\Api\Http\Request;
use Sunnydevbox\TWInventory\Events\InventoryTransactionEvent;

class InventoryController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWInventory\Repositories\Inventory\InventoryRepository $repository,
		\Sunnydevbox\TWInventory\Transformers\InventoryTransformer $transformer,
		\Sunnydevbox\TWInventory\Validators\InventoryValidator $validator,
		\Sunnydevbox\TWInventory\Services\InventoryService $service
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
		$this->service = $service;
	}


	public function store(Request $request)
	{
		try {
			$this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
 
			$result = $this->service->store($request);
 
			return $this->response->item($result, $this->transformer);
 
		 } catch (ValidatorException $e) {
 
			 return response()->json([
				 'status_code'   => 400,
				 'message' =>$e->getMessageBag()
			 ], 400);
		 }
	}

	public function update(Request $request, $id)
	{
		try {
			
           	$this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
		   
			$result = $this->service->update($request, $id);
			event(new InventoryTransactionEvent('update', $result));
            return $this->response->item($result, $this->transformer);

        } catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
        }
	}

}
