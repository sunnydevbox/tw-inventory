<?php

namespace Sunnydevbox\TWInventory\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class SettingController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWInventory\Repositories\Setting\SettingRepository $repository,
		\Sunnydevbox\TWInventory\Transformers\SettingTransformer $transformer,
		\Sunnydevbox\TWInventory\Validators\SettingValidator $validator
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
	}

}