<?php

namespace Sunnydevbox\TWInventory\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

class LocationController extends APIBaseController
{
	public function __construct(
		\Sunnydevbox\TWInventory\Repositories\Location\LocationRepository $repository,
		\Sunnydevbox\TWInventory\Transformers\LocationTransformer $transformer,
		\Sunnydevbox\TWInventory\Validators\LocationValidator $validator
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->validator = $validator;
	}

}