<?php 
namespace Sunnydevbox\TWInventory\Repositories\InventoryThreshold;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class InventoryThresholdRepository extends TWBaseRepository
{

    protected $fieldSearchable = [
        'id',
        'inventory_id',
        'location_id'
    ];


    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return 'Sunnydevbox\TWInventory\Models\InventoryThreshold';
    }


}

