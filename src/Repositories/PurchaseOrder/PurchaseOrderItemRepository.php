<?php 
namespace Sunnydevbox\TWInventory\Repositories\PurchaseOrder;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class PurchaseOrderItemRepository extends TWBaseRepository
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return null; "\Sunnydevbox\TWInventory\Validators\PurchaseOrderValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return '\Sunnydevbox\TWInventory\Models\PurchaseOrderItem';
    }
}

