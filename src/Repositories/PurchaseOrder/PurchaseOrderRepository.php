<?php 
namespace Sunnydevbox\TWInventory\Repositories\PurchaseOrder;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Sunnydevbox\TWInventory\Traits\PurchaseOrderCalculationTrait;

class PurchaseOrderRepository extends TWBaseRepository
{
    use PurchaseOrderCalculationTrait;

    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];

    public function generatePOCode($data)
    {
        // P-{random number}-{supplier_id}-{sequence}

        $random = str_pad(rand(0, 9999), 5, "0", STR_PAD_LEFT);
        $sequence = $this->makeModel()->count() + 1;
        $supplier_id = $data['supplier_id'];

        return "P-{$random}-{$supplier_id}-{$sequence}";
    }


    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWInventory\Validators\PurchaseOrderValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return '\Sunnydevbox\TWInventory\Models\PurchaseOrder';
    }
    
    public function boot()
    {
        parent::boot();
        $this->pushCriteria(\Sunnydevbox\TWInventory\Criteria\PurchaseOrderCriteria::class);
    }
}

