<?php 
namespace Sunnydevbox\TWInventory\Repositories\PurchaseOrder;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class PurchaseOrderHistoryRepository extends TWBaseRepository
{

    public function validator()
    {
        return null;
        
    }
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return '\Sunnydevbox\TWInventory\Models\PurchaseOrderHistory';
    }
}

