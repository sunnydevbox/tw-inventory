<?php 
namespace Sunnydevbox\TWInventory\Repositories\Inventory;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class InventoryHistoryRepository extends TWBaseRepository
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];

    protected $fieldSearchable = [];

    
    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return null; // "\Sunnydevbox\TWInventory\Validators\InventoryValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return 'Sunnydevbox\TWInventory\Models\InventoryHistory';
    }

    function boot()
    {
        parent::boot();
        $this->pushCriteria('\Sunnydevbox\TWCore\Criteria\SearchRelationshipsCriteria');
    }
}

