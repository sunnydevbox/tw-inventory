<?php 
namespace Sunnydevbox\TWInventory\Repositories\Inventory;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class InventoryRepository extends TWBaseRepository
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];

    protected $fieldSearchable = [
        'id',
        'name',
        'description',
        'is_assembly',
    ];

    
    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWInventory\Validators\InventoryValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return 'Sunnydevbox\TWInventory\Models\Inventory';
    }

    function boot()
    {
        parent::boot();
        $this->pushCriteria('\Sunnydevbox\TWCore\Criteria\SearchRelationshipsCriteria');
    }
}

