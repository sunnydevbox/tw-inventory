<?php 
namespace Sunnydevbox\TWInventory\Repositories\Customer;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;


class CustomerRepository extends TWBaseRepository
{

    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];

    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWInventory\Validators\CustomerValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return '\Sunnydevbox\TWInventory\Models\Customer';
    }
}

