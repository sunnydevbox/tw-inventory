<?php 
namespace Sunnydevbox\TWInventory\Repositories\SaleOrder;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Sunnydevbox\TWInventory\Traits\SaleOrderCalculationTrait;

class SaleOrderRepository extends TWBaseRepository
{

    use SaleOrderCalculationTrait;

    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];

    public function generateSOCode($data)
    {
        // P-{random number}-{supplier_id}-{sequence}

        $random = str_pad(rand(0, 9999), 5, "0", STR_PAD_LEFT);
        $sequence = $this->makeModel()->count() + 1;
        $supplier_id = $data['customer_id'];

        return "S-{$random}-{$supplier_id}-{$sequence}";
    }


    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWInventory\Validators\SaleOrderValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return '\Sunnydevbox\TWInventory\Models\SaleOrder';
    }
}

