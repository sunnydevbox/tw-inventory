<?php 
namespace Sunnydevbox\TWInventory\Repositories\SaleOrder;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class SaleOrderHistoryRepository extends TWBaseRepository
{

    public function validator()
    {
        return null;
        
    }
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return '\Sunnydevbox\TWInventory\Models\SaleOrderHistory';
    }
}

