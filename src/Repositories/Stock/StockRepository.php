<?php 
namespace Sunnydevbox\TWInventory\Repositories\Stock;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class StockRepository extends TWBaseRepository
{

    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];

    protected $fieldSearchable = [
        'id',
        'inventory_id',
        'location_id',
    ];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWInventory\Validators\StockValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return '\Sunnydevbox\TWInventory\Models\Stock';
    }

    function boot()
    {
        parent::boot();
        $this->pushCriteria('\Sunnydevbox\TWCore\Criteria\SearchRelationshipsCriteria');
        $this->pushCriteria('\Sunnydevbox\TWInventory\Criteria\StockCriteria');
    }
}

