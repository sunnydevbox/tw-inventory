<?php 
namespace Sunnydevbox\TWInventory\Repositories\Location;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;


class LocationRepository extends TWBaseRepository
{

    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWInventory\Validators\LocationValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return 'Stevebauman\Inventory\Models\Location';
    }
}

