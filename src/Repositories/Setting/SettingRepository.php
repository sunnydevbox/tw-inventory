<?php
namespace Sunnydevbox\TWInventory\Repositories\Setting;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class SettingRepository extends TWBaseRepository
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = ['paginate'];


    public function getValue($key = null)
    {
        if ($key) {
            $kv = $this->model->where('id', $key)->select('value')->first();
            return (isset($kv->value) ? $kv->value : '');
        }

        return false;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWInventory\Validators\SettingValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return '\Sunnydevbox\TWInventory\Models\Setting';
    }
}
