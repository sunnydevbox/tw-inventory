<?php 
namespace Sunnydevbox\TWInventory\Repositories\Manufacturer;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class ManufacturerRepository extends TWBaseRepository
{
    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = ['paginate'];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWInventory\Validators\ManufacturerValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return '\Sunnydevbox\TWInventory\Models\Manufacturer';
    }
}

