<?php

namespace Sunnydevbox\TWInventory\Transformers;

use League\Fractal\TransformerAbstract;

use Sunnydevbox\TWInventory\Transformers\LocationTransformer;
use Sunnydevbox\TWInventory\Transformers\TransactionTransformer;
use Sunnydevbox\TWInventory\Transformers\StockMovementTransformer;
use Sunnydevbox\TWInventory\Transformers\InventoryTransformer;
use Sunnydevbox\TWInventory\Models\Stock;

class StockTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'item',
        'location',
        'transactions',
        'movements',
        'lastMovement',
    ];

    protected $defaultIncludes =   [
        // 'location', 
        // 'movements',
        // 'transactions',
    ];

    public function transform($obj)
    {
        $data =  $obj->toArray();        
        return $data;

        return [
            'id'            => (int) $obj->id,
            'user'          => $obj->user,
            'inventory'     => $obj->inventory,
            
            'location'      => $obj->location,
            
            'quantity'      => $obj->quantity,
            'aisle'         => $obj->aisle,
            'row'           => $obj->row,
            'bin'           => $obj->bin,

            'item'          => $obj->item,
            'movements'      => $obj->movements,
            'transactions'   => $obj->transactions,
        ];
    }

    public function includeItem($obj)
    {
        if ($obj->item) {

            return $this->item($obj->item, new InventoryTransformer);
        }

        return null;
    }

    public function includeLocation(Stock $obj)
    {
        if ($obj->location) {

            return $this->item($obj->location, new LocationTransformer);
        }

        return null;
    }

    public function includeTransactions(Stock $obj)
    {
        if ($obj->transaction) {

            return $this->collection($obj->transaction, new TransactionTransformer);
        }

        return null;
    }

    public function includeMovements($obj)
    {
        if ($obj->movements) {
            return $this->collection($obj->movements, new StockMovementTransformer);
        }

        return null;
    }

    public function includeLastMovement($obj) 
    {
        return $this->collection($obj->getLastMovement, new StockMovementTransformer);
    }
}