<?php

namespace Sunnydevbox\TWInventory\Transformers;

use League\Fractal\TransformerAbstract;
use Sunnydevbox\TWInventory\Models\Supplier;

class SupplierTransformer extends TransformerAbstract
{
    private $mode = 'simple'; // simple | complete

    public function transform(Supplier $obj)
    {
        if (app('request')->get('filter')) {
            $data =  $obj->toArray();
            
            return $data;
        }

        $data =  [
            'id'            => (int) $obj->id,
            'name'          => $obj->name,
        ];


        if ($this->mode == 'complete') {

            $data += [
                'address'       => $obj->address,
                'zip_code'      => $obj->zip_code,
                'region'        => $obj->region,
                'city'          => $obj->city,
                'country'       => $obj->country,
                'contact_name'  => $obj->contact_name,
                'contact_phone' => $obj->contact_phone,
                'contact_fax'   => $obj->contact_fax,
                'contact_email' => $obj->contact_email,
            ];
        }

        return $data;
    }


    public function setMode($mode)
    {
        $this->mode = $mode;
    }
}