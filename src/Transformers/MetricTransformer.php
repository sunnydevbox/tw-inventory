<?php

namespace Sunnydevbox\TWInventory\Transformers;

use League\Fractal\TransformerAbstract;
use Sunnydevbox\TWInventory\Models\Metric;

class MetricTransformer extends TransformerAbstract
{

    public function transform(Metric $obj)
    {
        return [
            'id'        => (int) $obj->id,
            'name' 	    => $obj->name,
            'symbol'    => $obj->symbol,
        ];
    }
}