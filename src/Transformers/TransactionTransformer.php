<?php

namespace Sunnydevbox\TWInventory\Transformers;

use League\Fractal\TransformerAbstract;
use Sunnydevbox\TWInventory\Models\Transaction;

class TransactionTransformer extends TransformerAbstract
{

    public function transform(Transaction $obj)
    {
        return [
            'id'    		=> (int) $obj->id,
            'user' 			=> $obj->user,
            'stocks'		=> $obj->stocks,
            'quantity'		=> $obj->quantity,
        ];
    }
}