<?php

namespace Sunnydevbox\TWInventory\Transformers;

use League\Fractal\TransformerAbstract;
use Sunnydevbox\TWInventory\Models\Location;

class LocationTransformer extends TransformerAbstract
{

    public function transform($obj)
    {
        $data =  $obj->toArray();
        return $data;
    }
}