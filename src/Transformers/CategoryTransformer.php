<?php

namespace Sunnydevbox\TWInventory\Transformers;

use League\Fractal\TransformerAbstract;
use Sunnydevbox\TWInventory\Models\Category;

class CategoryTransformer extends TransformerAbstract
{

    public function transform(Category $obj)
    {
        return [
            'id'    => (int) $obj->id,
            'name' 	=> $obj->name,            
        ];
    }
}