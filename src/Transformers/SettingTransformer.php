<?php

namespace Sunnydevbox\TWInventory\Transformers;

use League\Fractal\TransformerAbstract;

class SettingTransformer extends TransformerAbstract
{
    protected $defaultIncludes =   [
    ];

    public function transform($obj)
    {
        $data =  $obj->toArray();        
        return $data;
    }
}