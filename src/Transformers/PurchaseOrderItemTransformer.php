<?php

namespace Sunnydevbox\TWInventory\Transformers;

use League\Fractal\TransformerAbstract;

class PurchaseOrderItemTransformer extends TransformerAbstract
{
    protected $defaultIncludes =   [
    ];

    public function transform($obj)
    {
        $data =  $obj->toArray();        
        return $data;
    }
}