<?php

namespace Sunnydevbox\TWInventory\Transformers;

use League\Fractal\TransformerAbstract;

use Sunnydevbox\TWInventory\Transformers\LocationTransformer;
use Sunnydevbox\TWInventory\Transformers\TransactionTransformer;
use Sunnydevbox\TWInventory\Transformers\StockMovementTransformer;

class PurchaseOrderTransformer extends TransformerAbstract
{
    protected $defaultIncludes =   [
    ];

    public function transform($obj)
    {
        $data =  $obj->toArray();        
        return $data;
    }
}