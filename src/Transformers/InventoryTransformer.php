<?php

namespace Sunnydevbox\TWInventory\Transformers;

use League\Fractal\TransformerAbstract;
use Sunnydevbox\TWInventory\Models\Inventory;
use Sunnydevbox\TWInventory\Transformers\CategoryTransformer;
use Sunnydevbox\TWInventory\Transformers\MetricTransformer;
use Sunnydevbox\TWInventory\Transformers\SkuTransformer;
use Sunnydevbox\TWInventory\Transformers\StockTransformer;
use Sunnydevbox\TWInventory\Transformers\SupplierTransformer;

class InventoryTransformer extends TransformerAbstract
{
    protected $availableIncludes =   [
        'parent',
        'sku',
        'stocks',
        'suppliers',
    ];

    protected $defautIncludes = [
        'category', 
        'metric',
    ];

    public function transform(Inventory $obj)
    {
        $data =  $obj->toArray();

        $data['total_stocks'] = $obj->getTotalStock();
        
        return $data;
    }


    public function includeCategory(Inventory $obj)
    {
        if ($obj->category) {

            return $this->item($obj->category, new CategoryTransformer);
        }

        return null;
    }

    public function includeMetric($obj)
    {
        if ($obj->metric) {

            return $this->item($obj->metric, new MetricTransformer);
        }

        return null;
    }

    public function includeParent($obj)
    {
        if ($obj->parent) {

            return $this->item($obj->parent, new InventoryTransformer);
        }

        return null;
    }

    public function includeSku($obj)
    {
        if ($obj->sku) {

            return $this->item($obj->sku, new SkuTransformer);
        }

        return null;
    }

    public function includeStocks($obj)
    {
        if ($obj->stocks) {

            return $this->collection($obj->stocks, new StockTransformer);
        }

        return null;
    }


    public function includeSuppliers($obj)
    {
        if ($obj->suppliers) {

            $S =  new SupplierTransformer;
            $S->setMode('complete');

            return $this->collection($obj->suppliers, $S);
        }

        return null;
    }
}