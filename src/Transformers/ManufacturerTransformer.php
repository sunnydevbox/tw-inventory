<?php

namespace Sunnydevbox\TWInventory\Transformers;

use League\Fractal\TransformerAbstract;
use Sunnydevbox\TWInventory\Models\Manufacturer;

class ManufacturerTransformer extends TransformerAbstract
{
    public function transform(Manufacturer $obj)
    {
        return $obj->toArray();
    }
}