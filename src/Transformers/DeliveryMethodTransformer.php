<?php

namespace Sunnydevbox\TWInventory\Transformers;

use League\Fractal\TransformerAbstract;
use Sunnydevbox\TWInventory\Models\Category;

class DeliveryMethodTransformer extends TransformerAbstract
{

    public function transform($obj)
    {
        return $obj->toArray();
    }
}