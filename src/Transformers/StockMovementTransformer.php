<?php

namespace Sunnydevbox\TWInventory\Transformers;

use League\Fractal\TransformerAbstract;
use Sunnydevbox\TWInventory\Models\StockMovement;

class StockMovementTransformer extends TransformerAbstract
{

    public function transform(StockMovement $obj)
    {
        $data =  $obj->toArray();    
        return $data;
        
        return [
            'id'            => (int) $obj->id,
            'user'          => $obj->user,
            'inventory'     => $obj->inventory,
            'location'      => $obj->location,
            'quantity'      => $obj->quantity,
            'aisle'         => $obj->aisle,
            'row'           => $obj->row,
            'bin'           => $obj->bin,
        ];
    }
}