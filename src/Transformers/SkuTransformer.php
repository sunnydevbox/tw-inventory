<?php

namespace Sunnydevbox\TWInventory\Transformers;

use League\Fractal\TransformerAbstract;
use Sunnydevbox\TWInventory\Models\Sku;

class SkuTransformer extends TransformerAbstract
{

    public function transform(Sku $obj)
    {
        $data =  $obj->toArray();
        
        return $data;

        return [
            'id'    		=> (int) $obj->id,
            'code' 			=> $obj->code,
            'inventory'		=> $obj->inventory
        ];
    }
}