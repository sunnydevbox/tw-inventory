<?php
namespace Sunnydevbox\TWInventory;

use Illuminate\Support\ServiceProvider;
use App\Console\Kernel;

class TWInventoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application events.
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/api.php');
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->registerProviders();
        $this->registerCommands();

        // COPIES THE config file to Laravel's config folder
        $this->publishes([
            __DIR__ . '/../config/config.php' => config_path('tw-inventory.php'),
        ], 'config');

        // ASSIGNS the alias
        $this->mergeConfigFrom(
            __DIR__ . '/../config/config.php', 'tw-inventory'
        );
    }

    protected function registerProviders()
    {

        /**
         * ADD SCHEDULING FOR THRESHOLD
         * TO DO :
         * ADD EMAIL NOTIFICATION
         * POPULATE INVENTORY PER BRANCH TABLE FOR ITS THRESHOLD AND TOTAL QUANTITY
         */
        if ( class_exists('\Sunnydevbox\TWInventory\Console\Kernel') ) {

            $this->app->singleton('sunnydevbox.twinventory.console.kernel', function($app) {
                $dispatcher = $app->make(\Illuminate\Contracts\Events\Dispatcher::class);
                return new \Sunnydevbox\TWInventory\Console\Kernel($app, $dispatcher);
            });
            
            $this->app->make('sunnydevbox.twinventory.console.kernel');
        }
        

        // EVENTS
        if (class_exists('\Sunnydevbox\TWInventory\EventServiceProvider')
            && !$this->app->resolved('\Sunnydevbox\TWInventory\EventServiceProvider')
        ) {
            $this->app->register(\Sunnydevbox\TWInventory\EventServiceProvider::class);    
        }

        if (class_exists('\Sunnydevbox\TWUser\TWUserServiceProvider')
            && !$this->app->resolved('\Sunnydevbox\TWUser\TWUserServiceProvider')) {
            $this->app->register(\Sunnydevbox\TWUser\TWUserServiceProvider::class);
        }

        if (class_exists('\Stevebauman\Inventory\InventoryServiceProvider')
            && !$this->app->resolved('\Stevebauman\Inventory\InventoryServiceProvider')) {
            $this->app->register(\Stevebauman\Inventory\InventoryServiceProvider::class);
        }

        if (class_exists('\Baum\Providers\BaumServiceProvider')
            && !$this->app->resolved('\Baum\Providers\BaumServiceProvider')) {
            $this->app->register(\Baum\Providers\BaumServiceProvider::class);
        }

        $this->loadViewsFrom(__DIR__.'/Views/Email', 'emailView');

        $this->publishes([
            __DIR__.'/Views/Email' => resource_path('/Views/Email/emailView'),
        ]);
    }


    protected function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            
            $this->commands([
                \Sunnydevbox\TWInventory\Console\Commands\InstallInventoryCommand::class,
                \Sunnydevbox\TWInventory\Console\Commands\PublishConfigCommand::class,
                \Sunnydevbox\TWInventory\Console\Commands\MigrateCommand::class,
                \Sunnydevbox\TWInventory\Console\Commands\NotificationCommand::class,
            ]);
        }
    }

}