<?php
namespace Sunnydevbox\TWInventory\Console;

use App\Console\Kernel as ConsoleKernel;
use Illuminate\Console\Scheduling\Schedule;


class Kernel extends ConsoleKernel
{ 
    /**
     * Define the package's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        parent::schedule($schedule);

        $schedule->command('twinventory:notify')->twiceDaily(7,17);

    }



}