<?php

namespace Sunnydevbox\TWInventory\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\Artisan;

class InstallInventoryCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twinventory:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish TWInventory migrations files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        
        $this->info('Installing INVENTORY SYSTEM ...');
        $exitCode = Artisan::call('inventory:install');
        $this->info('... DONE');
    }

    public function fire()
    {
        echo 'fire';
    }
}
