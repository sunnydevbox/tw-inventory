<?php

namespace Sunnydevbox\TWInventory\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\Artisan;
use Sunnydevbox\TWInventory\Services\InventoryNotificationService;

class NotificationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twinventory:notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish TWInventory Notification Email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->info('Running notification email...');

        $service = app('Sunnydevbox\TWInventory\Services\InventoryNotificationService');
        
        $service->sendAllNotificaton();
        
    }

    public function fire()
    {
        echo 'fire';
    }
}
