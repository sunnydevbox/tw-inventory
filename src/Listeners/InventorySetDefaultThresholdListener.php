<?php

namespace Sunnydevbox\TWInventory\Listeners;

use Sunnydevbox\TWInventory\Events\InventoryTransactionEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sunnydevbox\TWInventory\Repositories\Setting\SettingRepository;

class InventorySetDefaultThresholdListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        SettingRepository $rpoSetting
    ) {
        $this->rpoSetting = $rpoSetting;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event)
    {
        $threshold = $this->rpoSetting->getValue(1);

        $event->inventory->qty_threshold = $threshold;
        $event->inventory->save();
    }
}
