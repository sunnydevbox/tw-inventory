<?php

namespace Sunnydevbox\TWInventory\Listeners;

use Sunnydevbox\TWInventory\Events\SaleOrderItemCreatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sunnydevbox\TWInventory\Repositories\SaleOrder\SaleOrderRepository;

class SaleOrderRecalculateListener
{
    private $rpoSaleOrder = null;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(SaleOrderRepository $rpoSaleOrder)
    {
        $this->rpoSaleOrder = $rpoSaleOrder;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event) {
        // dd($event->saleOrderItem);
        $SO = $this->rpoSaleOrder->calculateItemTotals($event->saleOrderItem);

        \Log::info('sale_order_total_udpated', ['so_id' => $SO->id, 'total' => $SO->total, 'total' => $SO->sub_total, 'items_count' => $SO->items_count]);
    }
}
