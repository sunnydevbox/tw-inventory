<?php

namespace Sunnydevbox\TWInventory\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sunnydevbox\TWInventory\Repositories\Inventory\InventoryHistoryRepository;

class InventoryTransactionListener
{
    private $rpoInventoryHistory = null;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(InventoryHistoryRepository $rpoInventoryHistory)
    {
        $this->rpoInventoryHistory = $rpoInventoryHistory;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event) {
       // dd($event);
        //$PO = $this->rpoPurchaseOrder->calculateItemTotals($event->purchaseOrderItem->purchaseOrder);
        // $this->rpoInventoryHistory->create();
        // \Log::info('Inventory');
    }
}
