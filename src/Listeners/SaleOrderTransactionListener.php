<?php

namespace Sunnydevbox\TWInventory\Listeners;

use Sunnydevbox\TWInventory\Events\SaleOrderItemCreatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sunnydevbox\TWInventory\Repositories\SaleOrder\SaleOrderHistoryRepository;

class SaleOrderTransactionListener
{
    private $rpoSaleOrderHistory = null;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(SaleOrderHistoryRepository $rpoSaleOrderHistory)
    {
        $this->rpoSaleOrderHistory = $rpoSaleOrderHistory;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event) {
        //dd($event);
        $SO = $this->rpoSaleOrderHistory->create([
            'action' => $event->action,
            'user_id' => $event->currentUser,
            //'description' => 
            'sale_order_id' => $event->saleOrder->id,
        ]);
            
        \Log::info('so_histo');
    }
}
