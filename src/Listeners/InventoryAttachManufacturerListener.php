<?php

namespace Sunnydevbox\TWInventory\Listeners;

use Sunnydevbox\TWInventory\Events\InventoryTransactionEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sunnydevbox\TWInventory\Repositories\Manufacturer\InventoryManufacturerRepository;
use Sunnydevbox\TWInventory\Repositories\Manufacturer\ManufacturerRepository;

class InventoryAttachManufacturerListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        ManufacturerRepository $rpoManufacturer,
        InventoryManufacturerRepository $rpoInventoryManufacturer
    ) {
        $this->rpoManufacturer = $rpoManufacturer;
        $this->rpoInventoryManufacturer = $rpoInventoryManufacturer;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(InventoryTransactionEvent $event)
    {
        // VERIFY first if manufacturer is valid
        if (is_null(request('manufacturer_id')) || $this->rpoManufacturer->find(request('manufacturer_id'))) {
            $result = $this->rpoInventoryManufacturer->updateOrCreate([
                'inventory_id'      => $event->inventory->id,
            ],
            [
                'inventory_id'      => $event->inventory->id,
                'manufacturer_id'   => request('manufacturer_id'),
            ]);
        }
    }
}
