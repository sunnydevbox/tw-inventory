<?php

namespace Sunnydevbox\TWInventory\Listeners;

use Sunnydevbox\TWInventory\Events\SaleOrderItemCreatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sunnydevbox\TWInventory\Services\SaleOrderService;

class SaleOrderTransactionHandleItemsListener
{
    private $service = null;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct( SaleOrderService $service )
    {   
        $this->service = $service;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event) {

        switch ($event->saleOrder->status) {
            case 'complete' : 
                $this->service->markSoldItems($event->saleOrder->id);
            break; 
            case 'unapproved' : 
                $this->service->markCancelItems($event->saleOrder->id);
            break; 
        }

    }
}
