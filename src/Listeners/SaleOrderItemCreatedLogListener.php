<?php

namespace Sunnydevbox\TWInventory\Listeners;

use Sunnydevbox\TWInventory\Events\SaleOrderItemCreatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SaleOrderItemCreatedLogListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(SaleOrderItemCreatedEvent $event)
    {
        \Log::info('sale_order_item_created', ['soi' => $event->saleOrderItem->toArray()]);
    }
}
