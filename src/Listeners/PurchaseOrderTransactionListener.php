<?php

namespace Sunnydevbox\TWInventory\Listeners;

use Sunnydevbox\TWInventory\Events\PurchaseOrderItemCreatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sunnydevbox\TWInventory\Repositories\PurchaseOrder\PurchaseOrderHistoryRepository;

class PurchaseOrderTransactionListener
{
    private $rpoPurchaseOrderHistory = null;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(PurchaseOrderHistoryRepository $rpoPurchaseOrderHistory)
    {
        $this->rpoPurchaseOrderHistory = $rpoPurchaseOrderHistory;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event) {
        //dd($event);
        $PO = $this->rpoPurchaseOrderHistory->create([
            'action' => $event->action,
            'user_id' => $event->currentUser,
            //'description' => 
            'purchase_order_id' => $event->purchaseOrder->id,
        ]);

        \Log::info('po_histo');
    }
}
