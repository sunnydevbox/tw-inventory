<?php

namespace Sunnydevbox\TWInventory\Listeners;

use Sunnydevbox\TWInventory\Events\PurchaseOrderItemCreatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PurchaseOrderItemCreatedLogListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(PurchaseOrderItemCreatedEvent $event)
    {
        \Log::info('purchase_order_item_created', ['poi' => $event->purchaseOrderItem->toArray()]);
    }
}
