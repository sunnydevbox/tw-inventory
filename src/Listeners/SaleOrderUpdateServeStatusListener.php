<?php

namespace Sunnydevbox\TWInventory\Listeners;

use Sunnydevbox\TWInventory\Events\SaleOrderItemCreatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sunnydevbox\TWInventory\Repositories\SaleOrder\SaleOrderRepository;

class SaleOrderUpdateServeStatusListener
{
    private $rpoSaleOrder = null;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(SaleOrderRepository $rpoSaleOrder)
    {
        $this->rpoSaleOrder = $rpoSaleOrder;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event) {
        if ($event->saleOrderItem->saleOrder ) {
            // item_count > 
        }

        // $PO = $this->rpoPurchaseOrder->calculateItemTotals($event->purchaseOrderItem->purchaseOrder);

        // \Log::info('purchase_order_total_udpated', ['po_id' => $PO->id, 'total' => $PO->total, 'total' => $PO->sub_total, 'items_count' => $PO->items_count]);
    }
}
