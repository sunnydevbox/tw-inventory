<?php

namespace Sunnydevbox\TWInventory\Listeners;

use Sunnydevbox\TWInventory\Events\PurchaseOrderItemCreatedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sunnydevbox\TWInventory\Repositories\PurchaseOrder\PurchaseOrderRepository;

class PurchaseOrderUpdateServeStatusListener
{
    private $rpoPurchaseOrder = null;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(PurchaseOrderRepository $rpoPurchaseOrder)
    {
        $this->rpoPurchaseOrder = $rpoPurchaseOrder;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event) {
        if ($event->purchaseOrderItem->purchaseOrder ) {
            // item_count > 
        }

        // $PO = $this->rpoPurchaseOrder->calculateItemTotals($event->purchaseOrderItem->purchaseOrder);

        // \Log::info('purchase_order_total_udpated', ['po_id' => $PO->id, 'total' => $PO->total, 'total' => $PO->sub_total, 'items_count' => $PO->items_count]);
    }
}
