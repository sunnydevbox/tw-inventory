<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('code')->unique(); // S.O. code
            $table->integer('user_id')->nullable()->unsigned();
            $table->integer('customer_id')->unsigned();
            $table->integer('location_id')->unsigned();

            $table->string('status')->nullable();
            $table->text('notes')->nullable();

            // SHipping method
            // terms
            // delivery date
            $table->timestamp('delivery_date')->nullable();
            $table->timestamp('order_date')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales'); 
    }
}
