<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sale_customers', function($table) {
            $table->dropForeign('sale_customers_sale_id_foreign');
            $table->dropForeign('sale_customers_customer_id_foreign');

            $table->dropIndex('sale_customers_sale_id_index');
            $table->dropIndex('sale_customers_customer_id_index');
        });

        Schema::dropIfExists('sale_customers'); 
        Schema::dropIfExists('customers'); 
    }
}
