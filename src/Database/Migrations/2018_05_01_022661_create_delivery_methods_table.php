<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_methods', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->softDeletes();
        });

        Schema::create('purchase_order_delivery_methods', function($table) {
            $table->increments('id')->unsigned();
            
            $table->integer('delivery_method_id')->unsigned();
            $table->integer('purchase_order_id')->unsigned();

            $table->index('delivery_method_id');
            $table->index('purchase_order_id');

            $table->foreign('delivery_method_id')
                ->references('id')
                ->on('delivery_methods');

            $table->foreign('purchase_order_id')
                ->references('id')
                ->on('purchase_orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_order_delivery_methods', function($table) {
            $table->dropForeign('purchase_order_delivery_methods_delivery_method_id_foreign');
            $table->dropForeign('purchase_order_delivery_methods_purchase_order_id_foreign');

            $table->dropIndex('purchase_order_delivery_methods_delivery_method_id_index');
            $table->dropIndex('purchase_order_delivery_methods_purchase_order_id_index');
        });

        Schema::dropIfExists('purchase_order_delivery_methods'); 
        Schema::dropIfExists('delivery_methods'); 
    }
}
