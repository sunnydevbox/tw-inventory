<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryThresholdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('inventory_thresholds', function (Blueprint $table) {
            
            $table->increments('id');
            $table->integer('inventory_id')->unsigned();
            $table->integer('location_id')->unsigned();
            $table->decimal('qty_threshold',8,2);
            $table->decimal('qty_total',8,2);
            $table->timestamps();

            $table->index('inventory_id');
            $table->foreign('inventory_id')
                ->references('id')
                ->on('inventories');

            $table->index('location_id');
            $table->foreign('location_id')
                ->references('id')
                ->on('locations');
            
            // $table->primary(['inventory_transaction_id', 'location_id']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('inventory_thresholds', function (Blueprint $table) {
            
            $table->dropForeign('inventory_thresholds_inventory_id_foreign');
            $table->dropIndex('inventory_threshold_inventory_id_index');

            $table->dropForeign('inventory_thresholds_location_id_foreign');
            $table->dropIndex('inventory_thresholds_location_id_index');

        });

        Schema::dropIfExists('inventory_thresholds');

    }
}
