<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventories', function (Blueprint $table) {
            // PACKAGE-related cols
            $table->boolean('packaging_applies')->nullable();
            $table->integer('package_unit_id')->unsigned()->nullable();
            $table->integer('package_qty_per_package')->nullable();

            $table->index('package_unit_id');
            $table->foreign('package_unit_id')
                ->references('id')
                ->on('metrics');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventories', function($table) {
            $table->dropForeign('inventories_package_unit_id_foreign');

            $table->dropIndex('inventories_package_unit_id_index');

            $table->dropColumn(['package_unit_id', 'package_qty_per_package', 'packaging_applies']);
        });
    }
}
