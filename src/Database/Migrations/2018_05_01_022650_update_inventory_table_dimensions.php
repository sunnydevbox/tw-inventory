<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateInventoryTableDimensions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventories', function (Blueprint $table) {
            $table->boolean('expires')->nullable()->default(false); // Determins if item has shelf life

            $table->decimal('dimension_length', 8, 2)->nullable()->defaut(0);
            $table->decimal('dimension_width', 8, 2)->nullable()->defaut(0);
            $table->decimal('dimension_height', 8, 2)->nullable()->defaut(0);

            $table->decimal('weight', 8, 2)->nullable()->default(0);
        });

        Schema::table('purchase_orders', function (Blueprint $table) {
            $table->string('delivery_name')->nullable();
            $table->string('delivery_address')->nullable();
            $table->string('delivery_city')->nullable();
            $table->string('delivery_region')->nullable();
            $table->string('delivery_zip')->nullable();
            $table->string('delivery_country')->default('PH');

            $table->decimal('tax_total', 8, 2)->default(0);
            $table->decimal('sub_total', 8, 2)->default(0);
            $table->decimal('total', 8, 2)->default(0);
            $table->decimal('items_count', 8, 2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_orders', function($table) {
            $table->dropColumn(['total', 'sub_total', 'items_count']);
        });

        Schema::table('inventories', function($table) {
            $table->dropColumn([
                'expires', 
                'dimension_length', 
                'dimension_width', 
                'dimension_height', 
                'weight', 
                'weight_unit'
            ]);
        });
        
        
    }
}
