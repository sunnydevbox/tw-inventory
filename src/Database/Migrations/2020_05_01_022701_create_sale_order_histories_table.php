<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleOrderHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('sale_order_histories', function (Blueprint $table) {
            
            $table->increments('id')->unsigned();
            $table->integer('user_id')->nullable()->unsigned();
            $table->integer('sale_order_id')->nullable()->unsigned();

            $table->string('description')->nullable();
            $table->string('action')->nullable();

            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('sale_order_histories', function($table) {
            $table->dropForeign('sale_order_histories_sale_order_id_foreign');
            $table->dropIndex('sale_order_histories_sale_order_id_index');
        });

        
        Schema::dropIfExists('sale_order_histories');

    }
}
