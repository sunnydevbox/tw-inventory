<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManufacturersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manufacturers', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name');

            $table->softDeletes();
        });

        Schema::create('inventory_manufacturers', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->integer('inventory_id')->unsigned();
            $table->integer('manufacturer_id')->unsigned()->nullable();

            $table->timestamps();


            $table->index('inventory_id');
            $table->index('manufacturer_id');

            $table->foreign('inventory_id')
                ->references('id')
                ->on('inventories')
                ->onDelete('cascade');

            $table->foreign('manufacturer_id')
                ->references('id')
                ->on('manufacturers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventory_manufacturers', function($table) {
            $table->dropForeign('inventory_manufacturers_inventory_id_foreign');
            $table->dropForeign('inventory_manufacturers_manufacturer_id_foreign');

            $table->dropIndex('inventory_manufacturers_inventory_id_index');
            $table->dropIndex('inventory_manufacturers_manufacturer_id_index');
        });

        Schema::dropIfExists('inventory_manufacturers'); 
        Schema::dropIfExists('manufacturers'); 
    }
}
