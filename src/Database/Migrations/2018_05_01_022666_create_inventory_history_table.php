<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_histories', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            
            $table->integer('inventory_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->string('action');
            $table->text('description');

            $table->timestamps();
            $table->softDeletes();
            
            $table->index('inventory_id');
            $table->index('user_id');

            $table->foreign('inventory_id')
                ->references('id')
                ->on('inventories')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventory_histories', function($table) {
            $table->dropForeign('inventory_histories_inventory_id_foreign');
            $table->dropForeign('inventory_histories_user_id_foreign');

            $table->dropIndex('inventory_histories_inventory_id_index');
            $table->dropIndex('inventory_histories_user_id_index');
        });

        Schema::dropIfExists('inventory_histories'); 
    }
}
