<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('code')->unique(); // P.O. code
            $table->integer('user_id')->nullable()->unsigned();
            $table->integer('supplier_id')->unsigned();
            $table->integer('location_id')->unsigned();

            $table->string('status')->nullable();
            $table->text('notes')->nullable();

            // SHipping method
            // terms
            // delivery date
            $table->timestamp('delivery_date')->nullable();
            $table->timestamp('order_date')->nullable();

            $table->softDeletes();
            $table->timestamps();

            $table->index('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->index('supplier_id');
            $table->foreign('supplier_id')
                ->references('id')
                ->on('suppliers');

            $table->index('location_id');
            $table->foreign('location_id')
                ->references('id')
                ->on('locations');
        });

        Schema::create('purchase_order_items', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->integer('purchase_order_id')->unsigned();
            $table->integer('user_id')->nullable()->unsigned();
            $table->integer('inventory_transaction_id')->unsigned();
            $table->decimal('quantity', 8, 2)->default(0);
            $table->decimal('price', 8, 2)->default(0);
            $table->decimal('total', 8, 2)->default(0);
            $table->text('notes')->nullable();

            $table->timestamps();

            $table->index('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->index('purchase_order_id');
            $table->foreign('purchase_order_id')
                ->references('id')
                ->on('purchase_orders')
                ->onUpdate('restrict')
                ->onDelete('cascade');

                $table->index('inventory_transaction_id');
                $table->foreign('inventory_transaction_id')
                    ->references('id')
                    ->on('inventory_transactions')
                    ->onUpdate('restrict')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_order_items', function($table) {
            $table->dropForeign('purchase_order_items_purchase_order_id_foreign');
            $table->dropIndex('purchase_order_items_purchase_order_id_index');

            $table->dropForeign('purchase_order_items_user_id_foreign');
            $table->dropIndex('purchase_order_items_user_id_index');

            $table->dropForeign('purchase_order_items_inventory_transaction_id_foreign');
            $table->dropIndex('purchase_order_items_inventory_transaction_id_index');
        });

        Schema::table('purchase_orders', function($table) {
            $table->dropForeign('purchase_orders_user_id_foreign');
            $table->dropForeign('purchase_orders_supplier_id_foreign');
            $table->dropForeign('purchase_orders_location_id_foreign');
            $table->dropIndex('purchase_orders_user_id_index');
            $table->dropIndex('purchase_orders_supplier_id_index');
            $table->dropIndex('purchase_orders_location_id_index');
        });
        
        Schema::dropIfExists('purchase_order_items');
        Schema::dropIfExists('purchase_orders');
    }
}
