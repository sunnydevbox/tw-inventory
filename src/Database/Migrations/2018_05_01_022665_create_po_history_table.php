<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePOHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order_histories', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            
            $table->integer('purchase_order_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->string('action');
            $table->text('description');

            $table->timestamps();
            $table->softDeletes();
            
            $table->index('purchase_order_id');
            $table->index('user_id');

            $table->foreign('purchase_order_id')
                ->references('id')
                ->on('purchase_orders')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_order_histories', function($table) {
            $table->dropForeign('purchase_order_histories_purchase_order_id_foreign');
            $table->dropForeign('purchase_order_histories_user_id_foreign');

            $table->dropIndex('purchase_order_histories_purchase_order_id_index');
            $table->dropIndex('purchase_order_histories_user_id_index');
        });

        Schema::dropIfExists('purchase_order_histories'); 
    }
}
