<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSaleOrdersAddFieldTotalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sale_orders', function (Blueprint $table) {
            
            $table->float('tax_total')->nullable();
            $table->float('items_count')->nullable();
            $table->float('total')->nullable();
            $table->float('sub_total')->nullable();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('sale_orders', function($table) {
 
            $table->dropColumn(['tax_total']);
            $table->dropColumn(['items_count']);
            $table->dropColumn(['total']);
            $table->dropColumn(['sub_total']);
        });

    }
}
