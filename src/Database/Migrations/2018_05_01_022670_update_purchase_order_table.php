<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePurchaseOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_orders', function (Blueprint $table) {
            $table->string('invoice_number')->nullable();

            $table->integer('delivery_method_id')->unsigned()->nullable();
            $table->integer('payment_term_id')->unsigned()->nullable();

            $table->index('delivery_method_id');
            $table->index('payment_term_id');

            $table->foreign('delivery_method_id')
                ->references('id')
                ->on('delivery_methods');

            $table->foreign('payment_term_id')
                ->references('id')
                ->on('payment_terms');;
        });

        Schema::table('purchase_order_items', function($table) {
            // $table->
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_orders', function($table) {
            $table->dropForeign('purchase_orders_delivery_method_id_foreign');
            $table->dropForeign('purchase_orders_payment_term_id_foreign');

            $table->dropIndex('purchase_orders_delivery_method_id_index');
            $table->dropIndex('purchase_orders_payment_term_id_index');

            $table->dropColumn(['invoice_number', 'delivery_method_id', 'payment_term_id']);
        });
    }
}
