<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_terms', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('days');
            $table->string('type');
            
            $table->softDeletes();
        });

        Schema::create('purchase_order_payment_terms', function($table) {
            $table->increments('id')->unsigned();
            
            $table->integer('payment_term_id')->unsigned();
            $table->integer('purchase_order_id')->unsigned();

            $table->index('payment_term_id');
            $table->index('purchase_order_id');

            $table->foreign('payment_term_id')
                ->references('id')
                ->on('payment_terms');

            $table->foreign('purchase_order_id')
                ->references('id')
                ->on('purchase_orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_order_payment_terms', function($table) {
            $table->dropForeign('purchase_order_payment_terms_payment_term_id_foreign');
            $table->dropForeign('purchase_order_payment_terms_purchase_order_id_foreign');

            $table->dropIndex('purchase_order_payment_terms_payment_term_id_index');
            $table->dropIndex('purchase_order_payment_terms_purchase_order_id_index');
        });
        Schema::dropIfExists('purchase_order_payment_terms'); 
        Schema::dropIfExists('payment_terms'); 
    }
}
