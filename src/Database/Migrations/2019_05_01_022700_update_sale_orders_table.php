<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSaleOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sale_orders', function (Blueprint $table) {
            
            $table->integer('supplier_id')->nullable()->unsigned();
            $table->integer('delivery_method_id')->nullable()->unsigned();
            $table->integer('payment_term_id')->nullable()->unsigned();
            $table->integer('location_id')->nullable()->unsigned();

            $table->index('supplier_id');
            $table->foreign('supplier_id')
                ->references('id')
                ->on('suppliers');

            $table->index('delivery_method_id');
            $table->foreign('delivery_method_id')
                ->references('id')
                ->on('delivery_methods');

            $table->index('payment_term_id');
            $table->foreign('payment_term_id')
                ->references('id')
                ->on('payment_terms');

            $table->index('location_id');
            $table->foreign('location_id')
                ->references('id')
                ->on('locations');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('sale_orders', function($table) {
            
            $table->dropForeign('sale_orders_supplier_id_foreign');
            $table->dropForeign('sale_orders_delivery_method_id_foreign');
            $table->dropForeign('sale_orders_payment_term_id_foreign');
            $table->dropForeign('sale_orders_location_id_foreign');

            $table->dropColumn(['supplier_id']);
            $table->dropColumn(['delivery_method_id']);
            $table->dropColumn(['payment_term_id']);
            $table->dropColumn(['location_id']);
        });

    }
}
