<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->integer('user_id')->unsigned();

            $table->string('name');
            $table->string('address');
            $table->string('city');
            $table->string('region');
            $table->string('zip');
            $table->string('country')->default('PH');

            $table->timestamps();
            
            $table->index('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });


        Schema::create('business_users', function($table) {
            $table->increments('id')->unsigned();
            
            $table->integer('business_id')->unsigned();
            $table->string('email');
            $table->string('password');
            
            $table->timestamps();

            $table->index('business_id');
            $table->foreign('business_id')
                ->references('id')
                ->on('businesses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('businesses', function($table) {
            $table->dropForeign('businesses_user_id_foreign');
            $table->dropIndex('businesses_user_id_index');
        });

        Schema::dropIfExists('businesses'); 
    }
}
