<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_orders', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('code')->unique(); // Sales.O. code

            $table->string('po_number')->nullable(); // Purchase.O. code

            $table->string('status')->nullable();
            $table->text('notes')->nullable();

            $table->integer('customer_id')->nullable()->unsigned();
            $table->string('customer_name')->nullable();
            $table->string('customer_address')->nullable();
            $table->string('customer_city')->nullable();
            $table->string('customer_region')->nullable();
            $table->string('customer_zip')->nullable();
            $table->string('customer_country')->default('PH');

            $table->string('billing_name')->nullable();
            $table->string('billing_address')->nullable();
            $table->string('billing_city')->nullable();
            $table->string('billing_region')->nullable();
            $table->string('billing_zip')->nullable();
            $table->string('billing_country')->default('PH');

            $table->string('shipping_name')->nullable();
            $table->string('shipping_address')->nullable();
            $table->string('shipping_city')->nullable();
            $table->string('shipping_region')->nullable();
            $table->string('shipping_zip')->nullable();
            $table->string('shipping_country')->default('PH');


            // SHipping method
            // terms
            // delivery date
            $table->timestamp('delivery_date')->nullable();
            $table->timestamp('order_date')->nullable();

            $table->softDeletes();
            $table->timestamps();

            $table->index('customer_id');
            $table->foreign('customer_id')
                ->references('id')
                ->on('customers');
        });

        Schema::create('sale_order_items', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->integer('sale_order_id')->unsigned();
            $table->integer('user_id')->nullable()->unsigned();
            $table->integer('inventory_transaction_id')->unsigned();
            $table->decimal('quantity', 8, 2)->default(0);
            $table->decimal('price', 8, 2)->default(0);
            $table->decimal('total', 8, 2)->default(0);
            $table->text('notes')->nullable();

            $table->timestamps();

            $table->index('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->index('sale_order_id');
            $table->foreign('sale_order_id')
                ->references('id')
                ->on('sale_orders')
                ->onUpdate('restrict')
                ->onDelete('cascade');

                $table->index('inventory_transaction_id');
                $table->foreign('inventory_transaction_id')
                    ->references('id')
                    ->on('inventory_transactions')
                    ->onUpdate('restrict')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sale_order_items', function($table) {
            $table->dropForeign('sale_order_items_sale_order_id_foreign');
            $table->dropIndex('sale_order_items_sale_order_id_index');

            $table->dropForeign('sale_order_items_user_id_foreign');
            $table->dropIndex('sale_order_items_user_id_index');

            $table->dropForeign('sale_order_items_inventory_transaction_id_foreign');
            $table->dropIndex('sale_order_items_inventory_transaction_id_index');
        });

        Schema::table('sale_orders', function($table) {
            $table->dropForeign('sale_orders_customer_id_foreign');
            $table->dropIndex('sale_orders_customer_id_index');
        });
        
        Schema::dropIfExists('sale_order_items');
        Schema::dropIfExists('sale_orders');
    }
}
