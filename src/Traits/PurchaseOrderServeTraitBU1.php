<?php
namespace Sunnydevbox\TWInventory\Traits;

use Sunnydevbox\TWInventory\Traits\PurchaseOrderServeExpiryTrait;
use DB;

trait PurchaseOrderServeTraitBU1
{
    use PurchaseOrderServeExpiryTrait;

    public function serve($action, $poiId, $request)
    {
        // RECIEVE ALL or Partial
        // CANCEL ALL or partial
        // dd($request->all());
        
        
        $result = DB::transaction(function () use ($action, $poiId, $request){
            $poi = $this->rpoPurchaseOrderItem->find($poiId);
            
            // CREATE THE VARIANT
            $itemVariant = $poi->transaction->stock->item->newVariant();
            $itemVariant->name = 'Cherry Coke';
            $itemVariant->save();
            
            // CREATE THE STOCK ENTRY
            $stock = $itemVariant->newStockOnLocation($poi->transaction->stock->location);
            $stock->quantity = $request->get('quantity');
            $stock->cost = $request->get('cost  ');
            $stock->reason = 'I bought some';
            $stock->save();
            

            // CREATE THE TRANSACTION

            // CREATE THE POI ENTRY
            
            // dd($itemVariant);

            // // IF ITEM EXPIRES
            // if (false) { //$poi->transaction->stock->item->expires) {
            //     // LOGIC
            //     /**
            //      * IF ITEM has expiration
            //      * - create new stock record - a duplicate from the orig
            //      * -- should it auto destruct after the quantity is 0?
            //      * - create transaction record
            //      * - create new order item and attach this new stock
            //      * 
            //      */

            //     $copyStock = $poi->transaction->stock;
                
            //     // CHECK FOR DUPLICATE EXPIRATION DATE OF THIS STOCK FIRST
            //     $Stock = $copyStock->children()->whereExpiration($request->get('expiration_date'))->first();
            //     if (!$Stock) {
            //         $Stock = $this->rpoStock->makeModel();
            //         $Stock->inventory_id = $copyStock->item->id;
            //         $Stock->location_id = $copyStock->location->id;
            //         $Stock->aisle = $copyStock->aisle;
            //         $Stock->bin = $copyStock->bin;
            //         $Stock->row = $copyStock->row;
            //         $Stock->expiration_date = $request->get('expiration_date');
            //         $Stock->quantity = 0; //$request->get('quantity');
            //         $Stock->prependToNode($copyStock);
            //         // $Stock->cost = 0;// $request->get('price');
            //         // $Stock->reason = 'Creating new stock for ' . $request->get('expiration_date');
                    
            //         $Stock->save();
            //     }

            //     // CHECK IF the QTY is partial, full or over (equivalent to receive all)
            //     // if ($partial) {
            //         // $note = 'Partial delivery. Qty: $request->get('quantity');
            //     // } else if ( >= $totaQTy) {

            //     // }

            //     $transaction = $Stock->newTransaction('Received for QTY: ' . $request->get('quantity'));
            //     $transaction->ordered($request->get('quantity'));

            //     $transaction->received(
            //         $request->get('quantity'),
            //         'Received for ' . $request->get('quantity'),
            //         $request->get('price')
            //     );
    
            //     // ATTACH ITEM
            //     if (!$metric = $this->rpoMetric->find($copyStock->item->metric_id)) {
            //         throw new \InvalidArgumentException("Invalid unit");
            //     }
            //     $poiData = [
            //         'inventory_transaction_id'  => $transaction->id,
            //         'quantity'                  => $request->get('quantity'),
            //         'price'                     => $request->get('price'),
            //         'notes'                     => $request->get('notes'),
            //         'unit'                      => $metric->name,
            //         'purchase_order_id'         => $poi->purchaseOrder->id,
            //         'qty_served'                => $request->get('quantity'),
            //     ];
                
            //     $result = $this->rpoPurchaseOrderItem->create($poiData);

            //     // ($this->rpoPurchaseOrderItem->makeModel()->get()->toFlatTree());
            //     $poi->prependNode($result);

            //     event(new PurchaseOrderItemServedEvent($result));
                
            //     return ($result->purchaseOrder->items->toFlatTree());

            // } else {
            //     dd(1);
            // }
        });
        

        return $result;

        // $result = $poi->transaction->received(
        //     $request->get('quantity'), 
        //     $request->get('notes'), 
        //     $request->get('price')
        // );




        // // dd($poi->transaction->stock->item);
        // // dd($poi->transaction->getLastHistoryRecord());
       

        // $m = $poi->transaction->stock->getLastMovement();
        // $m->expiration_date = $request->get('expiration_date');
        // $m->save();

        switch($action) {
            case 'serve': 
            break;

        }
        //dd($action, $poiId);
    }
}