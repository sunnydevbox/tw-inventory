<?php
namespace Sunnydevbox\TWInventory\Traits;

use Sunnydevbox\TWInventory\Models\SaleOrder;
use Sunnydevbox\TWInventory\Models\SaleOrderItem;

trait SaleOrderCalculationTrait
{
    public function calculateItemTotals($saleOrder)
    {
        // $SOI = new SaleOrderItem();
        // if ($saleOrder instanceof $SOI) {
        //     $saleOrder = $saleOrder;
        // }

        if ( is_numeric($saleOrder) ) {
            $SO = $this->repository->find($saleOrder);
        } elseif ( is_numeric($saleOrder->sale_order_id) ) {
            $SO = $this->find($saleOrder->sale_order_id);
        } else {
            $SO = $saleOrder;
        }
        // $SO = (is_numeric($saleOrder)) ? $this->repository->find($saleOrder) : $saleOrder;

        // dd($SO->items);
        $items = $SO->items;

        $subTotal           = 0;
        $itemCount          = 0;
        $itemQuantityServed = 0;

        $parentId = null;

        foreach($items as $item) {
            $subTotal += $item->total;
            $itemCount += $item->quantity;

            // if ($item->parent_id) {
            //     $parentId = $item->parent_id;
            // }
        }

        // $parentQtyServed = 0;

        // if ($parentId) {
        //     $parent = ($items->where('id', $parentId)->first());

        //     $parent->qty_served = $parent->children()->get()->sum('qty_served');
        //     $parent->save();
        // }

        //dd($items->where('parent_id', null));
        //dd($parent->getSiblings());

        $SO->sub_total = $subTotal;
        $SO->items_count = $itemCount;

        // After sub-total
        $SO->tax_total = $this->calculateTaxTotal($SO);
        $SO->total = $this->calculateTotal($SO);

        $SO->update();

        return $SO;
    }

    public function calculateTotal($SO)
    {
        // CALCULATIONS HERE
        return $SO->sub_total + $SO->tax_total;
    }

    public function calculateTaxTotal($SO)
    {
        return ($SO->sub_total * $this->getTaxPercentage()) - $this->calculateDiscount();
    }

    public function calculateDiscount()
    {
        return 0;
    }


    public function getTaxPercentage()
    {
        return 0.12;
    }
}
