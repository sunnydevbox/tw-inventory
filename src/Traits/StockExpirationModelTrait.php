<?php
namespace Sunnydevbox\TWInventory\Traits;

trait StockExpirationModelTrait
{
    public function setExpirationDateAttribute($value)
    {
        $this->attributes['expiration_date'] = \Carbon\Carbon::createFromTimestamp(strtotime($value))->endOfDay();
    }
    
    public function scopeNotParent($query)
    {
        return $query->whereNull('parent');
    }

    public function scopeWhereExpiration($query, $date)
    {
        if (is_string($date)) {
            $date = \Carbon\Carbon::createFromTimestamp(strtotime($date))->endOfDay();
        }

        return $query->where('expiration_date', $date);
    }
}