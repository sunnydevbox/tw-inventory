<?php
namespace Sunnydevbox\TWInventory\Traits;

use Mail;

trait EmailNotificationTrait {

    // private $sendTo = $this->sendToNotification;

    public function mailNearExpiryNotification()
    {
        
        // $subject = "Stock Near Expiry Notification";

        $content = array(
            'stockList' => $this->nearExpiryList
        );

        // dd('emailView::threshold-notification');
        if ( $this->nearExpiryList ){
            Mail::send('emailView::near-expiry-notification', $content , function ($message) {
                $message->to($this->sendToNotification)
                  ->subject("Stock Near Expiry Notification");
            });

            return true;
        }

        return false;
        
    }

    public function mailExpiredStockesNotification()
    {
        
        // $subject = "Expired Stocks Notification";

        $content = array(
            'stockList' => $this->expiredList
        );

        // dd('emailView::threshold-notification');
        if ( $this->nearExpiryList ){
            Mail::send('emailView::expired-notification', $content , function ($message) {
                $message->to($this->sendToNotification)
                  ->subject("Expired Stocks Notification");
            });

            return true;
        }

        return false;
        
    }

    public function mailBelowThresholdNotification()
    {
        
        // $subject = "Stocks Below Threshold Notification";

        $content = array(
            'stockList' => $this->thresholdList
        );

        // dd('emailView::threshold-notification');
        if ( $this->nearExpiryList ){
            Mail::send('emailView::threshold-notification', $content , function ($message) {
                $message->to($this->sendToNotification)
                  ->subject("Stocks Below Threshold Notification");
            });

            return true;
        }

        return false;
        
    }

    public function mailAllNotification()
    {
        
        // $subject = "Stocks Below Threshold Notification";

        $content = array(
            'thresholdList' => $this->thresholdList,
            'expiredList' => $this->expiredList,
            'nearExpiryList' => $this->nearExpiryList,
        );

        // dd('emailView::threshold-notification');
        if ( $this->nearExpiryList ){
            Mail::send('emailView::all-notification', $content , function ($message) {
                $message->to($this->sendToNotification)
                  ->subject("Stocks Notification");
            });

            return true;
        }

        return false;
        
    }

}