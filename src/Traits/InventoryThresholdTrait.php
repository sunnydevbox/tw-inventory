<?php
namespace Sunnydevbox\TWInventory\Traits;


trait InventoryThresholdTrait {

    public function getBelowThreshold()
    {
        return $this->rpoInventoryThreshold
                        ->makeModel()
                        ->belowThreshold()
                        ->with('stock', 'location')
                        ->get();
    }


}