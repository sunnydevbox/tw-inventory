<?php
namespace Sunnydevbox\TWInventory\Traits;

use Sunnydevbox\TWInventory\Models\PurchaseOrder;
use Sunnydevbox\TWInventory\Models\PurchaseOrderItem;

trait PurchaseOrderCalculationTrait
{
    public function calculateItemTotals($purchaseOrder)
    {
        $POI = new PurchaseOrderItem();
        if ($purchaseOrder instanceof $POI) {
            $purchaseOrder = $purchaseOrder->purchaseOrder();
        }


        $PO = (is_numeric($purchaseOrder)) ? $this->repository->find($purchaseOrder) : $purchaseOrder;

        // $PO = $event->purchaseOrderItem->purchaseOrder;
        // dd($purchaseOrder);
        $items = $PO->items;

        $subTotal           = 0;
        $itemCount          = 0;
        $itemQuantityServed = 0;

        $parentId = null;

        foreach($items as $item) {
            $subTotal += $item->total;
            $itemCount += $item->quantity;

            if ($item->parent_id) {
                $parentId = $item->parent_id;
            }
        }

        $parentQtyServed = 0;

        if ($parentId) {
            $parent = ($items->where('id', $parentId)->first());

            $parent->qty_served = $parent->children()->get()->sum('qty_served');
            $parent->save();
        }

        //dd($items->where('parent_id', null));
        //dd($parent->getSiblings());

        $PO->sub_total = $subTotal;
        $PO->items_count = $itemCount;

        // After sub-total
        $PO->tax_total = $this->calculateTaxTotal($PO);
        $PO->total = $this->calculateTotal($PO);

        $PO->update();

        return $PO;
    }

    public function calculateTotal($PO)
    {
        // CALCULATIONS HERE
        return $PO->sub_total + $PO->tax_total;
    }

    public function calculateTaxTotal($PO)
    {
        return ($PO->sub_total * $this->getTaxPercentage()) - $this->calculateDiscount();
    }

    public function calculateDiscount()
    {
        return 0;
    }


    public function getTaxPercentage()
    {
        return 0.12;
    }
}
