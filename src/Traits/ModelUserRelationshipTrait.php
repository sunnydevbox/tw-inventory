<?php
namespace Sunnydevbox\TWInventory\Traits;

trait ModelUserRelationshipTrait
{
    public function user()
    {
        if (class_exists('Illuminate\Auth') || class_exists('Illuminate\Support\Facades\Auth')) {
            if (\Auth::check()) {
                return $this->belongsTo(get_class(\Auth::user()), 'user_id');
            }
        }
    }
    
}