<?php
namespace Sunnydevbox\TWInventory\Traits;

trait InventoryManufacturerModelTrait
{
    public function manufacturer()
    {
        return $this->hasOne(\Sunnydevbox\TWInventory\Models\InventoryManufacturer::class, 'inventory_id');
    }
}