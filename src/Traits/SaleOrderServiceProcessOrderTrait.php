<?php
namespace Sunnydevbox\TWInventory\Traits;

trait SaleOrderServiceProcessOrderTrait
{
    public function processOrder($action, $id)
    {
        $SO = $this->rpoSaleOrder->find($id);
        // dd($SO);
        switch($action) {
            case 'placed':
                $SO->placed();
            break;

            case 'parked':
                $SO->parked();
            break;

            case 'costed':
                $SO->costed();
            break;
            
            case 'receipted':
                $SO->receipted();
            break;

            case 'complete':
                $SO->complete();
            break;

            case 'deleted':
                $SO->deleted();
            break;

            case 'unapproved':
                $SO->unapproved();
            break;

            case 'prepared':
                $SO->prepared();
            break;
        }

        return $SO;
    }
}