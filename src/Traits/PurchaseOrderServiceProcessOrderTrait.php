<?php
namespace Sunnydevbox\TWInventory\Traits;

trait PurchaseOrderServiceProcessOrderTrait
{
    public function processOrder($action, $id)
    {
        $PO = $this->rpoPurchaseOrder->find($id);
        switch($action) {
            case 'placed':
                $PO->placed();
            break;

            case 'parked':
                $PO->parked();
            break;

            case 'costed':
                $PO->costed();
            break;
            
            case 'receipted':
                $PO->receipted();
            break;

            case 'complete':
                $PO->complete();
            break;

            case 'deleted':
                $PO->deleted();
            break;
        }

        return $PO;
    }
}