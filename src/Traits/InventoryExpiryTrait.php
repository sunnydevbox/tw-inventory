<?php
namespace Sunnydevbox\TWInventory\Traits;


trait InventoryExpiryTrait {

    public function getExpiredStocks()
    {
        return $this->rpoStock
                        ->makeModel()
                        ->expiredStocks()
                        ->with('item', 'location')
                        ->get();
    }

    public function getNearExpiryStocks()
    {
        return $this->rpoStock
                        ->makeModel()
                        ->nearExpiryStocks()
                        ->with('item', 'location')
                        ->get();
    }


}