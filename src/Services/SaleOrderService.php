<?php
namespace Sunnydevbox\TWInventory\Services;

use Sunnydevbox\TWInventory\Traits\SaleOrderServiceProcessOrderTrait;
use Sunnydevbox\TWInventory\Events\SaleOrderTransactionEvent;

class SaleOrderService
{
    use SaleOrderServiceProcessOrderTrait;

    public function store($data)
    {
        // Create PO and status is Draft
        $data['code'] = $this->rpoSaleOrder->generateSOCode($data);

        $data = $this->sanitize($data);

        $SO = $this->rpoSaleOrder->create($data);

        event(new SaleOrderTransactionEvent('so_status_parked', $SO));

        // event(new PurchaseOrderTransactionEvent('create', $PO));

        return $SO;
    }

    public function attachItem($data)
    {
        // dd($data);
        // TODO: CREATE/GET TRANSACTION ID
        // dd($data);
        $stock = $this->rpoStock->find($data['stock_id']);

        $transaction = $stock->newTransaction('Sale for QTY: ' . $data['quantity']);

        $transaction->reserved(
            $data['quantity'],
            'SOLD QTY : ' . $data['quantity'],
            $data['price']
        );

        // dd($transaction);
        $data['inventory_transaction_id'] = $transaction->id;
        // dd($data);
        $result = $this->saleOrderItemService->attachItem($data);
        
        return $result;
    }

    public function update($data, $id)
    {
        $data = $this->sanitize($data);

        $result = $this->rpoSaleOrder->update($data, $id);
        
        event(new SaleOrderTransactionEvent('so_update', $result));
        return $result;
    }

    public function sanitize($data)
    {
        $newData = [];
        foreach ($data as $field => $value) {
            if ($field == 'delivery_date' || $field == 'order_date') {
                
                    $newData[$field] = isset($value) ? \Carbon\Carbon::createFromTimestamp(strtotime($value)) : null;
                
            } else {
                $newData[$field] = $value;
            }
        } 

        return $newData;
    }

    public function removeItem($obj)
    {
        
        return $this->saleOrderItemService->removeItem($obj);
    }

    public function updateItem($data, $id)
    {
        return $this->saleOrderItemService->updateItem($data, $id);
    }

    public function markSoldItems($sID)
    {
        $SO = $this->rpoSaleOrder->find($sID);
        foreach ($SO->items as $item) {
            $item->transaction->sold();
        }
 
        return $SO;
    }

    public function markCancelItems($sID)
    {
        $SO = $this->rpoSaleOrder->find($sID);
        foreach ($SO->items as $item) {
            $item->transaction->cancel();
        }
 
        return $SO;
    }

    public function __construct(
        \Sunnydevbox\TWInventory\Repositories\Stock\StockRepository $rpoStock,
        \Sunnydevbox\TWInventory\Repositories\SaleOrder\SaleOrderRepository $rpoSaleOrder,
        \Sunnydevbox\TWInventory\Services\SaleOrderItemService $saleOrderItemService
    ) {
        $this->rpoStock = $rpoStock;
        $this->rpoSaleOrder = $rpoSaleOrder;
        $this->saleOrderItemService = $saleOrderItemService;
    }
}