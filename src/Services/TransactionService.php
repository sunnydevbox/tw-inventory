<?php
namespace Sunnydevbox\TWInventory\Services;


class TransactionService
{
    private $rpoTransaction;
    private $rpoStock;

    public function store($request)
    {
        // dd($request->all());

        $stock = $this->rpoStock->find($request->get('stock_id'));
        if ($stock) {
            

            $transaction = $stock->newTransaction('Sales Order');

            $transaction->reserved(5);

            echo $transaction->state; //Returns 'commerce-reserved'

            dd($transaction->isReserved()); // Returns true
            //dd($transaction);
        }
        

        // STORE inventory table
        // - SKU
        // - CATEGORy
        // - Supplier

        //dd($request->all());
        $inventory = $this->rpoInventory->create( $request->all() );
       
        if ($inventory) {
            $this->supplierService->addSupplier($inventory, $request->get('supplier_id'));
            $this->skuService->addSku($inventory, $request->get('sku'));
        }

        return $inventory;
    }

    public function update($request, $id)
    {
        $inventory = $this->rpoInventory->update($request->all(), $id);
        //$inventory = $this->repository->update($request->all(), $id);
       
        if ($inventory) {
            $this->supplierService->addSupplier($inventory, $request->get('supplier_id'));
            $this->skuService->addSku($inventory, $request->get('sku'));
        }
    }

    public function index($request)
	{	
		$limit = $request->get('limit') ? $request->get('limit') : config('repository.pagination.limit', 15);
        
        $result = $this->rpoInventory
                    ->paginate($limit);

        return $result;
    }

    public function releaseServe($id)
	{
 
        $stock = $this->rpoTransaction->find($id);

        if( $stock ){
            if ( !$stock->isReleased() ) {
                $stock->release( null, 'RELEASED - ' . $stock->name );
            } else {
                throw new \Exception("Transaction already released",400);
            }
        } else {
            throw new \Exception("Transaction Id does not exist",400);
        }
        
		return $stock;

    }

    public function releaseReserved($id)
	{
 
        $stock = $this->rpoTransaction->find($id);

        if( $stock ){
            if ( $stock->state != 'cancelled' ){
                $stock->cancel();
            }
            
        } else {
            throw new \Exception("Transaction Id does not exist",400);
        }
        
		return $stock;

    }

    // public function updateReserved($soi,$data){

    //     if ( $soi ) {
    //         $this->releaseReserved($soi->inventory_transaction_id);
    //         $transaction = $soi->newTransaction('Reserved for QTY: ' . $data['quantity']);

    //         $transaction->reserved(
    //             $data['quantity'],
    //             'Reserved for ' . $request->get('quantity'),
    //             $data['price']
    //         );

    //         return $transaction;
    //     }
    // }
    
    public function __construct(
        \Sunnydevbox\TWInventory\Repositories\Transaction\TransactionRepository $rpoTransaction,
        \Sunnydevbox\TWInventory\Repositories\Stock\StockRepository $rpoStock
    ) {

        $this->rpoTransaction = $rpoTransaction;
        $this->rpoStock = $rpoStock;
    }
}