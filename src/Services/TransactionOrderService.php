<?php
namespace Sunnydevbox\TWInventory\Services;


class TransactionOrderService
{
    private $rpoTransaction;
    private $rpoStock;

    public function store($request)
    {
        // dd($request->all());

        $stock = $this->rpoStock->find($request->get('stock_id'));
        if ($stock) {
            

            $transaction = $stock->newTransaction('Sales Order');

            $transaction->reserved(5);

            echo $transaction->state; //Returns 'commerce-reserved'

            dd($transaction->isReserved()); // Returns true
            //dd($transaction);
        }
        

        // STORE inventory table
        // - SKU
        // - CATEGORy
        // - Supplier

        //dd($request->all());
        $inventory = $this->rpoInventory->create( $request->all() );
       
        if ($inventory) {
            $this->supplierService->addSupplier($inventory, $request->get('supplier_id'));
            $this->skuService->addSku($inventory, $request->get('sku'));
        }

        return $inventory;
    }

    public function update($request, $id)
    {
        $inventory = $this->rpoInventory->update($request->all(), $id);
        //$inventory = $this->repository->update($request->all(), $id);
       
        if ($inventory) {
            $this->supplierService->addSupplier($inventory, $request->get('supplier_id'));
            $this->skuService->addSku($inventory, $request->get('sku'));
        }
    }

    public function index($request)
	{	
		$limit = $request->get('limit') ? $request->get('limit') : config('repository.pagination.limit', 15);
        
        $result = $this->rpoInventory
                    ->paginate($limit);

        return $result;
    }

    public function order($data)
    {
        // dd($data);
        $stock = $this->rpoStock->find($data['stock_id']);
        $transaction = $stock->newTransaction('Purchase Order Transaction');
        // dd($transaction);
        $reason = 'Purchase Order';

        $transaction->ordered($data['quantity'], $data['reason'], $data['cost']);
        return $transaction;
    }

    public function purchaseOrder($request)
    {
        // dd($request->all());
        $stock = $this->rpoStock->find(11);
        $transaction = $stock->newTransaction('Order Transaction');
        // dd($transaction);
        $reason = 'Purchase Order';
        $cost = 5;

        $transaction->ordered(5, $reason, $cost);

        echo $transaction->state; //Returns 'order-on-order'
                
        var_dump($transaction->isOrder()); // Returns true

        dd($transaction->received());
    }
    
    public function __construct(
        \Sunnydevbox\TWInventory\Repositories\Transaction\TransactionRepository $rpoTransaction,
        \Sunnydevbox\TWInventory\Repositories\Stock\StockRepository $rpoStock
    ) {

        $this->rpoTransaction = $rpoTransaction;
        $this->rpoStock = $rpoStock;
    }
}