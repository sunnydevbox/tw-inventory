<?php
namespace Sunnydevbox\TWInventory\Services;


class StockManagementService
{
    private $rpoInventory;
    private $rpoLocation;
    private $rpoStock;

    public function store($request)
    {
        $item = $this->rpoInventory->find($request->get('inventory_id'));
        $location = $this->rpoLocation->find($request->get('location_id'));
        // echo 'asd';
        $stock = $item->newStockOnLocation($location);
        $stock->quantity = $request->get('quantity');
        $stock->cost = $request->get('cost');
        $stock->reason = $request->get('reason');

        // OPTIONAL
        $stock->aisle = $request->get('aisle');
        $stock->row = $request->get('row');
        $stock->bin = $request->get('bin');

        $stock->expiration_date = $request->get('expiration_date');
        // dd($request);
        $stock->save();

        return $stock;
    }

    public function addStock($request)
    {
        $stock = $this->rpoStock->find($request->get('stock_id'));
        // $item = $this->rpoInventory->find($request->get('inventory_id'));
        // $location = $this->rpoLocation->find($request->get('location_id'));

        // $stock = $item->getStockFromLocation($location);

        /*
        * Reason and cost are always optional
        */
        $reason = $request->get('reason', '');
        $cost = $request->get('cost');
        $quantity = $request->get('quantity');

        /*
        * Or you can use the add, they both perform the same function
        */
        $stock->add($quantity, $reason, $cost);

        return $stock;
    }

    public function removeStock($request)
    {
        // $item = $this->rpoInventory->find($request->get('inventory_id'));
        // $location = $this->rpoLocation->find($request->get('location_id'));

        // $stock = $item->getStockFromLocation($location);

        $stock = $this->rpoStock->find($request->get('stock_id'));

        /*
        * Reason and cost are always optional
        */
        $reason = $request->get('reason', '');
        $quantity = $request->get('quantity');
        $cost = $request->get('cost');

        $stock->remove($quantity, $reason, $cost);

        return $stock;
    }

    public function update($request, $id)
    {
        $inventory = $this->rpoInventory->update($request->all(), $id);
        //$inventory = $this->repository->update($request->all(), $id);

        if ($inventory) {
            $this->supplierService->addSupplier($inventory, $request->get('supplier_id'));
            $this->skuService->addSku($inventory, $request->get('sku'));
        }
    }

    public function index($request)
	{
		$limit = $request->get('limit') ? $request->get('limit') : config('repository.pagination.limit', 15);

        $result = $this->rpoInventory
                    ->paginate($limit);

        return $result;
    }

    public function __construct(
        \Sunnydevbox\TWInventory\Repositories\Stock\StockRepository $rpoStock,
        \Sunnydevbox\TWInventory\Repositories\Inventory\InventoryRepository $rpoInventory,
        \Sunnydevbox\TWInventory\Repositories\Location\LocationRepository $rpoLocation
    ) {
        $this->rpoStock = $rpoStock;
        $this->rpoInventory = $rpoInventory;
        $this->rpoLocation = $rpoLocation;
    }
}
