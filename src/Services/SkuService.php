<?php
namespace Sunnydevbox\TWInventory\Services;

class SkuService
{
    private $rpoSku;

    public function addSku($inventory, $sku)
    {
        // IF an SKU is manually specified by the user
        if ($sku) {
            $sku = $inventory->createSku($sku, true);
        }

        return $sku;       
    }
    
    public function __construct(
        \Sunnydevbox\TWInventory\Repositories\Sku\SkuRepository $rpoSku
    ) {
        $this->rpoSku = $rpoSku;
    }
}