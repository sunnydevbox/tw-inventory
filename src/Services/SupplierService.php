<?php
namespace Sunnydevbox\TWInventory\Services;

class SupplierService
{
    private $rpoSupplier;

    public function addSupplier($inventory, $supplier_id)
    {
        // CHECK first if inventory already has this supplier
        // so that there will be NO duplicates
        if ($inventory->suppliers->where('id', $supplier_id)->count() == 0) {

            $supplier = $this->rpoSupplier->find($supplier_id);
            
            if ($supplier) {                
                return $inventory->addSupplier($supplier);
            }
        }

        return false;
    }
    
    public function __construct(
        \Sunnydevbox\TWInventory\Repositories\Supplier\SupplierRepository $rpoSupplier
    ) {
        $this->rpoSupplier = $rpoSupplier;
    }
}