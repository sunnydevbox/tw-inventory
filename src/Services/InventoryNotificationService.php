<?php
namespace Sunnydevbox\TWInventory\Services;

use Illuminate\Support\Facades\Schema;
use Sunnydevbox\TWInventory\Traits\EmailNotificationTrait;
use Sunnydevbox\TWInventory\Traits\InventoryExpiryTrait;
use Sunnydevbox\TWInventory\Traits\InventoryThresholdTrait;


class InventoryNotificationService
{
    use EmailNotificationTrait;
    use InventoryExpiryTrait;
    use InventoryThresholdTrait;

    private $rpoStock;
    private $rpoInventory;
    private $rpoInventoryThreshold;
    private $setting;

    private $thresholdList;
    private $expiredList;

    public function updateTableEntries() 
    {
 
        $items = $this->rpoStock
                        ->makeModel()
                        ->groupBy('inventory_id','location_id')
                        ->with('item')
                        ->get();

        foreach ( $items as $item ) {

            $i = $this->rpoStock
                    ->findWhere([   
                        'inventory_id' => $item->inventory_id, 
                        'location_id' => $item->location_id
                    ]);

  
            $data['qty_threshold']  = $item->item->qty_threshold; 
            $data['qty_total']      = $i->sum('quantity'); 
            $data['inventory_id']   = $item->inventory_id;
            $data['location_id']    = $item->location_id;

            $ipb = $this->rpoInventoryThreshold->findWhere(['inventory_id' => $item->inventory_id, 'location_id' => $item->location_id])->first();

            if ( !$ipb ){
                $this->rpoInventoryThreshold->create($data);
            } else {

                if ( $this->checkChanges($ipb,$data) ) {
                    $this->rpoInventoryThreshold->update($data,$ipb->id);                
                }
            }

        }
        
        return $this->rpoInventoryThreshold;
    }

    

    protected function checkChanges( $ipb, $data )
    {

        if (
            $ipb->qty_total == $data['qty_total'] &&
            $ipb->qty_threshold == $data['qty_threshold']
        ) {
            return false;
        }

        return true;
    }

    public function sendNearExpiryNotification()
    {
        // $this->sendNearExpiryNotification();
        if ( !$this->mailNearExpiryNotification() ) {
            throw new \Exception("Failed Sendng Near Expiry Notification");
        }

        return $this->nearExpiryList;
    }

    public function sendExpiredStocksNotification()
    {
        // $this->sendNearExpiryNotification();
        if ( !$this->mailExpiredStockesNotification() ) {
            throw new \Exception("Failed Sendng Expired Stocks Notification");
        }

        return $this->expiredList;
    }

    public function sendBelowThresholdNotification()
    {
        // $this->sendNearExpiryNotification();
        if ( !$this->mailBelowThresholdNotification() ) {
            throw new \Exception("Failed Sendng Below Threshold Notification");
        }

        // return $this->thresholdList;
    }

    public function sendAllNotificaton()
    {
        $this->mailAllNotification();

        return 1;
    }

    public function checkTableExist()
    {
        return Schema::hasTable('inventory_thresholds');
    }

    public function __construct(
        \Sunnydevbox\TWInventory\Repositories\Stock\StockRepository $rpoStock,
        \Sunnydevbox\TWInventory\Repositories\Inventory\InventoryRepository $rpoInventory,
        \Sunnydevbox\TWInventory\Repositories\InventoryThreshold\InventoryThresholdRepository $rpoInventoryThreshold,
        \Sunnydevbox\TWInventory\Repositories\Setting\SettingRepository $setting
    ) {

        // dd($rpoInventoryThreshold);
        $this->rpoStock = $rpoStock;
        $this->rpoInventory = $rpoInventory;
        $this->rpoInventoryThreshold = $rpoInventoryThreshold;
        $this->setting = $setting;

        // check table if exist
        // if ( $this->checkTableExist() ) {
        //     // UPDATE THRESHOLD TABLE
        //     $this->updateTableEntries();

        //     /**
        //      * GET LIST FOR NOTIFICATION
        //      */
        //     // dd(0);
        //     $this->thresholdList = $this->getBelowThreshold();
        //     $this->expiredList = $this->getExpiredStocks();
        //     $this->nearExpiryList = $this->getNearExpiryStocks();
        // }

        
  
    }

}