<?php
namespace Sunnydevbox\TWInventory\Services;

use Sunnydevbox\TWInventory\Events\PurchaseOrderItemDeletedEvent;
use Sunnydevbox\TWInventory\Events\PurchaseOrderItemUpdatedEvent;
use Sunnydevbox\TWInventory\Events\PurchaseOrderItemServedEvent;
use Sunnydevbox\TWInventory\Traits\PurchaseOrderServeTrait;
use Sunnydevbox\TWInventory\Service\TransactionService;
use DB;

class PurchaseOrderItemService
{
    use PurchaseOrderServeTrait;

    private $rpoPurchaseOrderItem;

    public function attachItem($item)
    {
        // return $item['metric_id'];
        // if ( isset($item['metric_id']) )
        if (isset($item['metric_id']) && !$metric = $this->rpoMetric->find($item['metric_id'])) {
            throw new \InvalidArgumentException("Invalid unit");
        }

        // foreach($items as $item) {
        // 1. Create inventory transaction per item
        // dd($this->validator);
        $this->validate->with($item)->passesOrFail('attached_item');

        $transaction = $this->transactionOrderService->order([
            'quantity'  => $item['quantity'],
            'cost'      => $item['price'],
            'reason'    => $item['notes'],
            'stock_id'  => $item['stock_id'],
        ]);

        $poiData = [
            // 'purchase_order_id'         => $item['purchase_order_id'],
            'inventory_transaction_id'  => $transaction->id,
            'quantity'                  => $item['quantity'],
            'price'                     => $item['price'],
            'notes'                     => $item['notes'],
            'unit'                      => $metric->name,
            // 'total' => ####, This is caluclated in the Model layer
        ];


        if (isset($item['purchase_order_id'])) {
            $poiData['purchase_order_id'] = $item['purchase_order_id'];
        }

        // 2. attach transacion to POItem
        $result = $this->rpoPurchaseOrderItem->create($poiData);

        // event(new PurchaseOrderItemCreatedEvent($result));

        return $result;
    }

    public function removeItem($obj)
    {
        // REMOVE TRANSACTION RECORD
        // THIS automatically removes the POI
        $status = $obj->transaction()->delete();

        event(new PurchaseOrderItemDeletedEvent($obj));

        return $status;
    }

    public function updateItem($data, $id)
    {
        // UPDATE TRANSACTION
        if ( $metric = $this->rpoMetric->find($data['unit']) ){
            $data['unit'] = $metric->name;
        }

        $result = $this->rpoPurchaseOrderItem->update($data, $id);

        event(new PurchaseOrderItemServedEvent($result));

        return $result;
    }

    public function __construct(
        \Sunnydevbox\TWInventory\Repositories\PurchaseOrder\PurchaseOrderItemRepository $rpoPurchaseOrderItem,
        \Sunnydevbox\TWInventory\Services\TransactionOrderService $transactionOrderService,
        \Sunnydevbox\TWInventory\Repositories\Metric\MetricRepository $rpoMetric,
        \Sunnydevbox\TWInventory\Repositories\Stock\StockRepository $rpoStock,
        \Sunnydevbox\TWInventory\Validators\PurchaseOrderItemValidator $validator,
        \Sunnydevbox\TWInventory\Services\TransactionService $transactionService
    ) {
        $this->rpoPurchaseOrderItem = $rpoPurchaseOrderItem;
        $this->transactionOrderService = $transactionOrderService;
        $this->rpoMetric = $rpoMetric;
        $this->rpoStock = $rpoStock;
        $this->validate = $validator;
        $this->transactionService = $transactionService;

    }
}
