<?php
namespace Sunnydevbox\TWInventory\Services;

use Sunnydevbox\TWInventory\Events\SaleOrderItemCreatedEvent;
use Sunnydevbox\TWInventory\Events\SaleOrderItemDeletedEvent;
use Sunnydevbox\TWInventory\Events\SaleOrderItemUpdatedEvent;

use DB;

class SaleOrderItemService
{


    public function attachItem($item)
    {

        // foreach($items as $item) {
        // 1. Create inventory transaction per item
        // dd($this->validator);
        // $this->validate->with($item)->passesOrFail('attached_item');
        if (isset($item['metric_id']) && !$metric = $this->rpoMetric->find($item['metric_id'])) {
            throw new \InvalidArgumentException("Invalid unit");
        }
        $item['unit'] = $metric->name;
        // 2. attach transacion to POItem
        // dd($item);
        $result = $this->rpoSaleOrderItem->create($item);
        // dd($result);
        event(new SaleOrderItemCreatedEvent($result));

        return $result;
    }

    public function removeItem($obj)
    {
        // REMOVE TRANSACTION RECORD
        // dd($obj);
        $this->transactionService->releaseReserved($obj->inventory_transaction_id);
        $status = $obj->transaction()->delete();
        // dd($obj);
        event(new SaleOrderItemDeletedEvent($obj));

        return $status;
    }

    public function updateItem($data, $id)
    {
        // GET SALE ORDER ITEM AND UPDATE 
        $SOI = $this->rpoSaleOrderItem->find($id);
        // dd($SOI->transaction->stock_id);
        // $stock = $this->rpoStock->find($SOI->transaction->stock_id);

        if ( $SOI ) {
            // release
            if ( $SOI->quantity != $data['quantity'] ) {

                $tempTransaction = $this->transactionService->releaseReserved($SOI->inventory_transaction_id);
    
                $stock = $this->rpoStock->find($SOI->transaction->stock_id);
    
                $transaction = $stock->newTransaction('Sale for QTY: ' . $data['quantity']);
                $transaction->reserved(
                    $data['quantity'],
                    'SOLD QTY : ' . $data['quantity'],
                    $data['price']
                );
    
                $data['inventory_transaction_id'] = $transaction->id;

                if ( !$transaction ) {
                    throw new \Exception("Failed Updating Inventory");
                }
            }

        } else {
            throw new \Exception("Sale Order Item not found!");
        }

        $result = $this->rpoSaleOrderItem->update($data, $id);

        event(new SaleOrderItemUpdatedEvent($result));
        // $transaction = $this->transactionService->releaseReserved($result->inventory_transaction_id);
        // event(new SaleOrderItemServedEvent($result));
            // echo 'asd';
            // dd($result);
        return $result;
    }

    public function addBox($soiId, $request)
    {
        $SOI = $this->rpoSaleOrderItem->find($soiId);
        // dd($request);
        $result = $this->rpoSaleOrderItem->update($request->all(), $soiId);

        return $result;
        // dd($soiId);
    }

    

    public function __construct(
        \Sunnydevbox\TWInventory\Repositories\SaleOrder\SaleOrderItemRepository $rpoSaleOrderItem,
        \Sunnydevbox\TWInventory\Services\TransactionService $transactionService,
        \Sunnydevbox\TWInventory\Repositories\Metric\MetricRepository $rpoMetric,
        \Sunnydevbox\TWInventory\Repositories\Stock\StockRepository $rpoStock
    ) {
        $this->rpoSaleOrderItem = $rpoSaleOrderItem;
        $this->transactionService = $transactionService;
        $this->rpoMetric = $rpoMetric;
        $this->rpoStock = $rpoStock;

    }
}
