<?php
namespace Sunnydevbox\TWInventory\Services;

class SupplementDataService
{
    public function _suppData($request)
    {
        $data = $request->get('data', []); 

        $datum = explode(';', strtolower($data));

        $return = [];
        foreach($datum as $d) {
            $d = strtolower($d);

            if ($d == 'delivery_methods') {
                $return[$d] = $this->rpoDeliveryMethod->all()->toArray();
            }

            if ($d == 'payment_terms') {
                $return[$d] = $this->rpoPaymentTerm->all()->toArray();
            }

            if ($d == 'suppliers') {
                $return[$d] = $this->rpoSupplier->all()->toArray();
            }

            if ($d == 'locations') {
                $return[$d] = $this->rpoLocation->all()->toArray();
            }

            if ($d == 'metrics') {
                $return[$d] = $this->rpoMetric->all()->toArray();
            }

            if ($d == 'manufacturers') {
                $return[$d] = $this->rpoManufacturer->all()->toArray();
            }

            if ($d == 'categories') {
                $return[$d] = $this->rpoCategory->all()->toArray();
            }
        }

        return $return;
    }

    public function __construct(
        \Sunnydevbox\TWInventory\Repositories\DeliveryMethod\DeliveryMethodRepository $rpoDeliveryMethod,
        \Sunnydevbox\TWInventory\Repositories\PaymentTerm\PaymentTermRepository $rpoPaymentTerm,
        \Sunnydevbox\TWInventory\Repositories\Location\LocationRepository $rpoLocation,
        \Sunnydevbox\TWInventory\Repositories\Supplier\SupplierRepository $rpoSupplier,
        \Sunnydevbox\TWInventory\Repositories\Manufacturer\ManufacturerRepository $rpoManufacturer,
        \Sunnydevbox\TWInventory\Repositories\Metric\MetricRepository $rpoMetric,
        \Sunnydevbox\TWInventory\Repositories\Category\CategoryRepository $rpoCategory
    ) {

        $this->rpoDeliveryMethod = $rpoDeliveryMethod;
        $this->rpoPaymentTerm = $rpoPaymentTerm;
        $this->rpoLocation = $rpoLocation;
        $this->rpoSupplier = $rpoSupplier;
        $this->rpoMetric = $rpoMetric;
        $this->rpoManufacturer = $rpoManufacturer;
        $this->rpoCategory = $rpoCategory;
    }
}