<?php
namespace Sunnydevbox\TWInventory\Services;

class ManufacturerService
{
    private $rpoManufacturer;

    public function addManufacturer($inventory, $manufacturer_id)
    {
        // IF an Manufacturer is manually specified by the user
        $manufacturer = $this->rpoInventoryManufacturer->updateOrCreate(
                            [
                                'inventory_id' => $inventory->id,
                            ],
                            [
                                'manufacturer_id' => $manufacturer_id,
                                'inventory_id' => $inventory->id,
                            ]
                        );
    }
    
    public function __construct(
        \Sunnydevbox\TWInventory\Repositories\Manufacturer\ManufacturerRepository $rpoManufacturer,
        \Sunnydevbox\TWInventory\Repositories\Manufacturer\InventoryManufacturerRepository $rpoInventoryManufacturer
    ) {
        $this->rpoManufacturer = $rpoManufacturer;
        $this->rpoInventoryManufacturer = $rpoInventoryManufacturer;
    }
}