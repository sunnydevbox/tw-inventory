<?php
namespace Sunnydevbox\TWInventory\Services;

use Sunnydevbox\TWInventory\Traits\PurchaseOrderServiceProcessOrderTrait;
use Sunnydevbox\TWInventory\Events\PurchaseOrderTransactionEvent;

class PurchaseOrderService
{
    use PurchaseOrderServiceProcessOrderTrait;


    private $purchaseOrderItemService;
    private $rpoPurchaseOrder;

    public function store($data)
    {
        // Create PO and status is Draft
        $data['code'] = $this->rpoPurchaseOrder->generatePOCode($data);

        $data = $this->sanitize($data);

        $PO = $this->rpoPurchaseOrder->create($data);

        // // Create records of the items for this PO
        // // Call POItems store
        // if (isset($data['items'])) {
        //     $this->purchaseOrderItemService->attachItems($PO, $data['items']);
        // }

        event(new PurchaseOrderTransactionEvent('create', $PO));

        return $PO;
    }

    public function update($data, $id)
    {
        $data = $this->sanitize($data);

        $result = $this->rpoPurchaseOrder->update($data, $id);
        
        event(new PurchaseOrderTransactionEvent('po_update', $result));
        return $result;
    }

    public function sanitize($data)
    {
        $newData = [];
        foreach ($data as $field => $value) {
            if ($field == 'delivery_date' || $field == 'order_date') {
                
                    $newData[$field] = isset($value) ? \Carbon\Carbon::createFromTimestamp(strtotime($value)) : null;
                
            } else {
                $newData[$field] = $value;
            }
        } 

        return $newData;
    }
    
    public function attachItem($data)
    {
        // $PO = $this->rpoPurchaseOrder->find($data['purchase_order_id']);
        $result = $this->purchaseOrderItemService->attachItem($data);
        
        return $result;
    }

    public function removeItem($obj)
    {
        
        return $this->purchaseOrderItemService->removeItem($obj);
    }

    public function updateItem($data, $id)
    {
        return $this->purchaseOrderItemService->updateItem($data, $id);
    }

    public function __construct(
        \Sunnydevbox\TWInventory\Repositories\PurchaseOrder\PurchaseOrderRepository $rpoPurchaseOrder,
        \Sunnydevbox\TWInventory\Services\PurchaseOrderItemService $purchaseOrderItemService
    ) {
        $this->rpoPurchaseOrder = $rpoPurchaseOrder;
        $this->purchaseOrderItemService = $purchaseOrderItemService;
    }
}