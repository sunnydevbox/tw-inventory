<?php
namespace Sunnydevbox\TWInventory\Services;


class InventoryService
{
    private $rpoInventory;
    private $supplierService;
    private $skuService;

    public function store($request)
    {
        // STORE inventory table
        // - SKU
        // - CATEGORy
        // - Supplier

        //dd($request->all());
        $inventory = $this->rpoInventory->create( $request->all() );
       
        if ($inventory) {
            $this->supplierService->addSupplier($inventory, $request->get('supplier_id'));
            $this->skuService->addSku($inventory, $request->get('sku'));
            // $this->
        }

        return $inventory;
    }

    public function update($request, $id)
    {
        $inventory = $this->rpoInventory->update($request->all(), $id);
        //$inventory = $this->repository->update($request->all(), $id);
       
        if ($inventory) {
            $this->supplierService->addSupplier($inventory, $request->get('supplier_id'));
            $this->skuService->addSku($inventory, $request->get('sku'));
        }

        return $inventory;
    }

    public function index($request)
	{	
		$limit = $request->get('limit') ? $request->get('limit') : config('repository.pagination.limit', 15);
        
        $result = $this->rpoInventory
                    ->paginate($limit);

        return $result;
    }
    
    public function __construct(
        \Sunnydevbox\TWInventory\Repositories\Inventory\InventoryRepository $rpoInventory,
        \Sunnydevbox\TWInventory\Services\SupplierService $supplierService,
        \Sunnydevbox\TWInventory\Services\SkuService $skuService
    ) {

        $this->rpoInventory = $rpoInventory;
        $this->supplierService = $supplierService;
        $this->skuService = $skuService;
    }
}