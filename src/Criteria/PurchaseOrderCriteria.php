<?php

namespace Sunnydevbox\TWInventory\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use JWTAuth;

use Dingo\Api\Http\Request;

//use Illuminate\Http\Request;

/**
 * Class UserEventCriteria.
 *
 * @package namespace App\Criteria;
 */
class PurchaseOrderCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        /**
         * items.children.transaction.stock.item
         * items.children.transaction.stock.item.sku
         * items.children.transaction.histories
         * items.children.transaction.stock.location
         * 
         */


        if ($model->first()->relationLoaded('items')) {

            $model = $model->with([
                'items' => function($query) {
                    $query->whereIsRoot();
                },
                'items.children' => function($query) {
                    $query->orderBy('created_at', 'desc');
                }
            ]);
        }
        // $model = $model->

        // $collection = $collection->each(function ($item, $key) {
        //     if (/* some condition */) {
        //         return false;
        //     }
        // });



        return $model;
    }
}
