<?php

namespace Sunnydevbox\TWInventory\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use JWTAuth;

use Dingo\Api\Http\Request;

//use Illuminate\Http\Request;

/**
 * Class UserEventCriteria.
 *
 * @package namespace App\Criteria;
 */
class StockCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $stockActions = app('request')->get('stockactions');
        $inStock = app('request')->get('instock');
        
        // dd($model->first()->getLastMovement());
        
        if ($stockActions) {
            // PATTERN: 
            // functionName1:param1|param2...;functionName2:param1|param2...
            // NOTE: params must be in the order that they are
            // set in the original method

            $actions = explode(';', $stockActions);
            
            foreach($actions as $action) {
                // $split = explode(':', $action);
                // $method = $split[0];
                // $params = isset($split[1]) ? explode('|', $split[1]) : [];

                // $model = call_user_func_array([$model, $method], $params);

                switch($action) {
                    case 'lastMovement': 
                        $model = $model->with(['movements' => function($query) {
                                $query->orderBy('created_at', 'desc')->limit(1);
                            }]  
                        );
                    break;
                }
            }
        }

        // SET TO TRUE TO FETCH AVAILABLE STOCK
        // EX instock=true
        if ( $inStock && $inStock === 'true' ) {
            $model = $model->where('quantity','>', '0' );
        }

        

        return $model;
    }
}
