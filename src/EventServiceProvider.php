<?php

namespace Sunnydevbox\TWInventory;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [

        // PURCHASE ORDER SAVED
        'Sunnydevbox\TWInventory\Events\PurchaseOrderSavedEvent' => [
            'Sunnydevbox\TWInventory\Listeners\PurchaseOrderRecalculateListener',
        ],

        // PURCHASE ORDER ITEM DELETED
        'Sunnydevbox\TWInventory\Events\PurchaseOrderItemDeletedEvent' => [
            'Sunnydevbox\TWInventory\Listeners\PurchaseOrderItemDeletedLogListener',
            'Sunnydevbox\TWInventory\Listeners\PurchaseOrderRecalculateListener',
        ],

        // PURCHASE ORDER ITEM CREATED
        'Sunnydevbox\TWInventory\Events\PurchaseOrderItemCreatedEvent' => [
            'Sunnydevbox\TWInventory\Listeners\PurchaseOrderItemCreatedLogListener',
            'Sunnydevbox\TWInventory\Listeners\PurchaseOrderRecalculateListener',
        ],

        // PURCHASE ORDER ITEM UPDATED
        'Sunnydevbox\TWInventory\Events\PurchaseOrderItemUpdatedEvent' => [
            // 'Sunnydevbox\TWInventory\Listeners\PurchaseOrderRecalculateListener',
        ],

        // PURCHASE ORDER ITEM SERVED
        'Sunnydevbox\TWInventory\Events\PurchaseOrderItemServedEvent' => [
            'Sunnydevbox\TWInventory\Listeners\PurchaseOrderRecalculateListener',
            'Sunnydevbox\TWInventory\Listeners\PurchaseOrderUpdateServeStatusListener',
        ],

        // PURCHASE ORDER LOG TRANSACTIONS
        'Sunnydevbox\TWInventory\Events\PurchaseOrderTransactionEvent' => [
            'Sunnydevbox\TWInventory\Listeners\PurchaseOrderTransactionListener',
        ],

        // SALE ORDER LOG TRANSACTIONS
        'Sunnydevbox\TWInventory\Events\SaleOrderTransactionEvent' => [
            'Sunnydevbox\TWInventory\Listeners\SaleOrderTransactionListener',
            'Sunnydevbox\TWInventory\Listeners\SaleOrderTransactionHandleItemsListener'
        ],


        // SALE ORDER ITEM RECALCULATE / CREATE TRANSACTIONS
        'Sunnydevbox\TWInventory\Events\SaleOrderItemCreatedEvent' => [
            'Sunnydevbox\TWInventory\Listeners\SaleOrderRecalculateListener'
        ],

        // SALE ORDER ITEM DELETED
        'Sunnydevbox\TWInventory\Events\SaleOrderItemDeletedEvent' => [
            'Sunnydevbox\TWInventory\Listeners\SaleOrderItemDeletedLogListener',
            'Sunnydevbox\TWInventory\Listeners\SaleOrderRecalculateListener'
        ],

        // SALE ORDER ITEM DELETED
        'Sunnydevbox\TWInventory\Events\SaleOrderItemUpdatedEvent' => [
            'Sunnydevbox\TWInventory\Listeners\SaleOrderRecalculateListener'
        ],

        // INVENTORY LOG TRANSACTIONS
        'Sunnydevbox\TWInventory\Events\InventoryTransactionEvent' => [
            'Sunnydevbox\TWInventory\Listeners\InventoryAttachManufacturerListener',
            'Sunnydevbox\TWInventory\Listeners\InventoryTransactionListener',
        ],

        'Sunnydevbox\TWInventory\Events\InventoryCreatedEvent' => [
            'Sunnydevbox\TWInventory\Listeners\InventorySetDefaultThresholdListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
