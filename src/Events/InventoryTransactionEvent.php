<?php

namespace Sunnydevbox\TWInventory\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class InventoryTransactionEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $inventory;
    public $action;
    public $currentUser;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($action, $inventory)
    {
        $this->action = $action;
        $this->inventory = $inventory;
        // $this->currentUser = $currentUser;
    }
}
