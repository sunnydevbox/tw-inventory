<?php

namespace Sunnydevbox\TWInventory\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use Auth;

class PurchaseOrderTransactionEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $purchaseOrder;
    public $action;
    public $currentUser;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($action, $purchaseOrder)
    {
        $user = Auth::user();

        $this->action = $action;
        $this->purchaseOrder = $purchaseOrder;
        $this->currentUser = $user ? $user->id : null;
    }
}
