<?php

namespace Sunnydevbox\TWInventory\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PurchaseOrderItemUpdatedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $purchaseOrderItem;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($purchaseOrderItem)
    {
        $this->purchaseOrderItem = $purchaseOrderItem;
    }
}
