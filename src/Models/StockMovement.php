<?php
namespace Sunnydevbox\TWInventory\Models;


use Stevebauman\Inventory\Models\InventoryStockMovement as SInventoryStockMovement;

class StockMovement extends SInventoryStockMovement
{
    protected $hidden = [
        // 'updated_at',
        'created_at',
        'deleted_at',
        'user_id',
    ];
    
	public function stock()
    {
        return $this->belongsTo('Sunnydevbox\TWInventory\Models\Stock', 'stock_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id', 'id');
    }

}