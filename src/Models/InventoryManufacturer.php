<?php
namespace Sunnydevbox\TWInventory\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

class InventoryManufacturer extends BaseModel
{
    protected $table = 'inventory_manufacturers';
    
    protected $hidden = [
        'deleted_at',
        'updated_at',
        'created_at',
    ];

    protected $fillable = [
        'inventory_id',
        'manufacturer_id',
    ];
}