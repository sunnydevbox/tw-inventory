<?php
namespace Sunnydevbox\TWInventory\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

use Sunnydevbox\TWInventory\Models\SaleOrderProcessOrderValidateTrait;

trait SaleOrderProcessOrderTrait
{   
    use SaleOrderProcessOrderValidateTrait;

    /**
     * PO is tagged ready for costing/approval from 
     * procurement department.
     */
    public function placed()
    {
        $this->validateProcess(self::STATE_SO_PLACED);

        $this->status = self::STATE_SO_PLACED;    
        $this->save();
    }

    /**
     * Draft version
     */
    public function parked()
    {
        $this->validateProcess(self::STATE_SO_PARKED);

        $this->status = self::STATE_SO_PARKED;    
        $this->save();
    }

    /**
     * Approver has approved the PO
     * and ready to be sent to supplier
     */
    public function costed()
    {
        $this->validateProcess(self::STATE_SO_COSTED);
        $this->status = self::STATE_SO_COSTED;    
        $this->save();
    }

    /**
     * Indicates that the PO is sent
     * to supplier via email or mail (printed version)
     */
    public function receipted()
    {
        $this->status = self::STATE_SO_RECEIPTED;    
        $this->save();
    }

    /**
     * Indicates that the PO 
     * has completed its lifecycle
     */
    public function complete()
    {
        $this->status = self::STATE_SO_COMPLETED;    
        $this->save();
    }

    public function deleteda()
    {
        $this->status = self::STATE_SO_DELETED;    
        $this->save();
    }

    public function unapproved()
    {
        $this->status = self::STATE_SO_UNAPPROVED;    
        $this->save();
    }

    public function prepared()
    {
        $this->status = self::STATE_SO_PREPARED;    
        $this->save();
    }
}