<?php
namespace Sunnydevbox\TWInventory\Models;


use Stevebauman\Inventory\Models\InventorySku as SSku;

class Sku extends SSku
{
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function item()
    {
        return $this->belongsTo('Sunnydevbox\TWInventory\Models\Inventory', 'inventory_id', 'id');
    }
}