<?php
namespace Sunnydevbox\TWInventory\Models;

use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\TWInventory\Models\HistoryTrackingTrait;

class PurchaseOrderHistory extends BaseModel
{
    use HistoryTrackingTrait;

    protected $fillable = [
        'purchase_order_id',
        'user_id',
        'descripion',
        'action',
    ];

    protected $table = 'purchase_order_histories';
}