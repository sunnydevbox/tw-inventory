<?php
namespace Sunnydevbox\TWInventory\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

use Sunnydevbox\TWInventory\Models\StateInterface;

trait PruchaseOrderProcessOrderValidateTrait
{   
    public function validateProcess($action)
    {
        switch(strtolower($action)) {

            case self::STATE_PO_PARKED: // DRAFT VERSION
                // 
            break;


            case self::STATE_PO_PLACED:
                // IF current status is PARKED or NULL.
                if (!$this->status || $this->status != self::STATE_PO_PARKED) {
                    throw new \League\Flysystem\Exception('po_invalid_current_status', 400);
                }

                if ($this->items_count == 0) {
                    throw new \League\Flysystem\Exception('po_invalid_items_count', 400);
                }
            break;


            case self::STATE_PO_UNAPPROVED:
                // IF current status is PARKED or NULL.
                if ($this->status != self::STATE_PO_PLACED) {
                    throw new \League\Flysystem\Exception('po_invalid_current_status', 400);
                }
            break;

            case self::STATE_PO_COSTED:
            break;

            case self::STATE_PO_RECEIPTED:
            break;

            case self::STATE_PO_PARTIAL_SERVED:
            break;

            case self::STATE_PO_COMPLETE:
            break;

            case self::STATE_PO_DELETED:
            break;
        }
    }
}