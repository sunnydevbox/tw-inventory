<?php
namespace Sunnydevbox\TWInventory\Models;


use Stevebauman\Inventory\Models\Supplier as SSupplier;

class Supplier extends SSupplier
{
    protected $hidden = [
        'created_at',
        'updated_at',
        'pivot',
    ];

	public function items()
    {
        return $this->belongsToMany('Sunnydevbox\TWInventory\Models\Inventory', 'inventory_suppliers', 'supplier_id')->withTimestamps();
    }
}