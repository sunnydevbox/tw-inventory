<?php
namespace Sunnydevbox\TWInventory\Models;

use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\TWInventory\Models\HistoryTrackingTrait;

class InventoryHistory extends BaseModel
{
    use HistoryTrackingTrait;

    protected $table = 'inventory_histories';

    // protected static function boot()
    // {
    //     parent::boot();

    //     echo 'boot inventory history trakcing';
    // }
}