<?php
namespace Sunnydevbox\TWInventory\Models;


use Stevebauman\Inventory\Models\Category as SCategory;

class Category extends SCategory
{
    protected $hidden = [
        'created_at',
        'updated_at',
        'user_id',
        'lft',
        'rgt',
        'belongs_to',
        'depth',
        'parent_id',
    ];

    public function inventories()
    {
        return $this->hasMany('Sunnydevbox\TWInventory\Models\Inventory', 'category_id', 'id');
    }
}