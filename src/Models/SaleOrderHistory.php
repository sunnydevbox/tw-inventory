<?php
namespace Sunnydevbox\TWInventory\Models;

use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\TWInventory\Models\HistoryTrackingTrait;

class SaleOrderHistory extends BaseModel
{
    use HistoryTrackingTrait;

    protected $fillable = [
        'sale_order_id',
        'user_id',
        'descripion',
        'action',
    ];

    protected $table = 'sale_order_histories';
}