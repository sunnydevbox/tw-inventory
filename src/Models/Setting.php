<?php
namespace Sunnydevbox\TWInventory\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

class Setting extends BaseModel
{
    public $timestamps = false;
    protected $fillable = [
        'key',
        'value',
    ];

    public function getValueAttribute()
    {
        return unserialize($this->attributes['value']);
    }

    public function setValueAttribute($value)
    {
        return $this->attributes['value'] = serialize($value);
    }
}