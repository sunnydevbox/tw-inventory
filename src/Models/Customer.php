<?php
namespace Sunnydevbox\TWInventory\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

class Customer extends BaseModel
{
    protected $hidden = [
        'created_at',
        'updated_at',
        'user_id',
        'lft',
        'rgt',
        'belongs_to',
        'depth',
        'parent_id',
    ];

    protected $fillable = [
        'name',
        'address',
        'postal_code',
        'zip_code',
        'region',
        'city',
        'country',
        'contact_title',
        'contact_name',
        'contact_phone',
        'contact_fax',
        'contact_email',
    ];

}