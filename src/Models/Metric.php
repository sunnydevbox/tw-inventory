<?php
namespace Sunnydevbox\TWInventory\Models;

use Stevebauman\Inventory\Models\Metric as MMetric;

class Metric extends MMetric
{
	protected $fillable = ['name', 'symbol'];
    protected $hidden = [
        'created_at',
        'updated_at',
        'user_id',
    ];
	public function items()
    {
        return $this->hasMany('Sunnydevbox\TWInventory\Models\Inventory', 'metric_id', 'id');
    }
}