<?php
namespace Sunnydevbox\TWInventory\Models;


use Stevebauman\Inventory\Models\InventoryTransactionHistory as SInventoryTransactionHistory;

class TransactionHistory extends SInventoryTransactionHistory
{
	public function transaction()
    {
        return $this->belongsTo(' Sunnydevbox\TWInventory\Models\Transaction', 'transaction_id', 'id');
    }
}