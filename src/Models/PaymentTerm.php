<?php
namespace Sunnydevbox\TWInventory\Models;
use Sunnydevbox\TWCore\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentTerm extends BaseModel
{
    use SoftDeletes;

    public $timestamps = false;
    protected $hidden = [
        'deleted_at'
    ];

    protected $fillable = [
        'name',
        'days',
        'type',
    ];
}