<?php
namespace Sunnydevbox\TWInventory\Models;

use Sunnydevbox\TWCore\Models\BaseModel;


class InventoryThreshold extends BaseModel
{   

    protected $table = 'inventory_thresholds';
    
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'inventory_id',
        'location_id',
        'qty_threshold',
        'qty_total'

    ];

    public function stock()
    {
        return $this->hasOne('Sunnydevbox\TWInventory\Models\Inventory', 'id', 'inventory_id');
    }

    public function location()
    {
        return $this->hasOne('Sunnydevbox\TWInventory\Models\Location', 'id', 'location_id');
    }

    public function isBelowThreshold()
    {
        return  ($this->qty_threshold >= $qty_total) ? true : false;
    }

    public function scopeBelowThreshold($query)
    {
        $query->whereRaw('qty_threshold >= qty_total');
    }


}
