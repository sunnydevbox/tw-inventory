<?php
namespace Sunnydevbox\TWInventory\Models;


use Stevebauman\Inventory\Models\Location as SLocation;

class Location extends SLocation
{
    protected $hidden = [
        'updated_at',
        'created_at',
        'deleted_at',
        'belongs_to',
        'parent_id',
        'lft',
        'rgt',
        'depth',
    ];

    public function stocks()
    {
        return $this->hasMany('Sunnydevbox\TWInventory\Models\Stock', 'location_id', 'id');
    }
}