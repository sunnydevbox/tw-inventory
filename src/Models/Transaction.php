<?php
namespace Sunnydevbox\TWInventory\Models;

use Stevebauman\Inventory\Models\InventoryTransaction as SIT;
use Sunnydevbox\TWInventory\Traits\ModelUserRelationshipTrait;

class Transaction extends SIT
{
    use ModelUserRelationshipTrait;
    
    protected $hidden = [
        'updated_at',
    ];
    
    public function stock()
    {
        return $this->belongsTo('Sunnydevbox\TWInventory\Models\Stock', 'stock_id', 'id');
    }

    public function histories()
    {
        return $this->hasMany('Sunnydevbox\TWInventory\Models\TransactionHistory', 'transaction_id', 'id');
    }
}