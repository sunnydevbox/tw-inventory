<?php
namespace Sunnydevbox\TWInventory\Models;

use Stevebauman\Inventory\Models\Inventory as SInventory;
use Sunnydevbox\TWInventory\Traits\InventoryManufacturerModelTrait;


class Inventory extends SInventory
{
    use InventoryManufacturerModelTrait;

    protected $table = 'inventories';

    protected $appends = [
        'total_stocks',
    ];
    
    public function category()
    {
        return $this->belongsTo('Sunnydevbox\TWInventory\Models\Category');
    }

    public function metric()
    {
        return $this->belongsTo('Sunnydevbox\TWInventory\Models\Metric');
    }

    public function sku()
    {
        return $this->hasOne('Sunnydevbox\TWInventory\Models\Sku', 'inventory_id', 'id');
    }

    public function stocks()
    {
        return $this->hasMany('Sunnydevbox\TWInventory\Models\Stock', 'inventory_id', 'id');
    }

    public function suppliers()
    {
        return $this->belongsToMany('Sunnydevbox\TWInventory\Models\Supplier', 'inventory_suppliers', 'inventory_id')->withTimestamps();
    }

    public function assemblies()
    {
        return $this->belongsToMany($this, 'inventory_assemblies', 'inventory_id', 'part_id')->withPivot(['quantity'])->withTimestamps();
    }

    public function getExpiresAttribute()
    {
        return $this->attributes['expires'] == 1 ? true : false;
    }

    public function getTotalStocksAttribute()
    {
        return $this->attributes['total_stocks'] = $this->getTotalStock();
    }

    public function getFillable()
    {
        return array_merge(parent::getFillable(), [
            'metric_id',
            'dimension_length',
            'dimension_width',
            'dimension_height',
            'metric_id',
            'expires',
            'weight',
            'weight_unit',
            'packaging_applies',
            'package_unit_id',
            'package_qty_per_package',
            'qty_threshold',
        ]);
    }


    public function __construct($attributes = []) 
    {
        parent::__construct($attributes);

        // $orig = $this->getFillable();

        // $fillable = [
        //     'expires',
        //     'dimension_length',
        //     'dimension_width',
        //     'dimension_height',
        //     'weight',
            
        //     'packaging_applies',
        //     'package_unit_id',
        //     'package_qty_per_package',
        // ];
        // // dd($orig);
        // $this->fillable(array_merge($fillable, $orig));
    }

    public static function boot()
    {
        parent::boot();
        
        Inventory::observe(new \Sunnydevbox\TWInventory\Observers\InventoryObserver);
    }

}