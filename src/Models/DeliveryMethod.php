<?php
namespace Sunnydevbox\TWInventory\Models;
use Sunnydevbox\TWCore\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryMethod extends BaseModel
{
    use SoftDeletes;
    protected $hidden = [
        'deleted_at'
    ];

    public $timestamps = false;

    protected $fillable = [
        'name',
    ];
}