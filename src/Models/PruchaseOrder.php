<?php
namespace Sunnydevbox\TWInventory\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

use Sunnydevbox\TWInventory\Models\StateInterface;
use Sunnydevbox\TWInventory\Models\PruchaseOrderProcessOrderTrait;

class PurchaseOrder extends BaseModel implements StateInterface
{   
    use PruchaseOrderProcessOrderTrait;

    
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'supplier_id',
        'user_id',
        'delivery_date',
        'order_date',
        'code',
        'notes',
        'location_id',
        'status',
        'total',
        'sub_total',
        'items_count',
        'invoice_number',

        'delivery_name',
        'delivery_address',
        'delivery_city',
        'delivery_region',
        'delivery_zip',
        'delivery_country',

        'delivery_method_id',
        'payment_term_id',
        'qty_served',
    ];

    public function items()
    {
        return $this->hasMany('Sunnydevbox\TWInventory\Models\PurchaseOrderItem', 'purchase_order_id', 'id');
    }

    public function supplier()
    {
        return $this->belongsTo('Sunnydevbox\TWInventory\Models\Supplier', 'supplier_id', 'id');
    }

    public function delivery_method()
    {
        return $this->belongsTo('Sunnydevbox\TWInventory\Models\DeliveryMethod', 'delivery_method_id', 'id');
    }

    public function payment_term()
    {
        return $this->belongsTo('Sunnydevbox\TWInventory\Models\PaymentTerm', 'payment_term_id', 'id');
    }

    protected static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->status = self::STATE_PO_PARKED;
        });
    }
}