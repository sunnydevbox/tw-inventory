<?php
namespace Sunnydevbox\TWInventory\Models;
use Kalnoy\Nestedset\NodeTrait;

use Stevebauman\Inventory\Models\InventoryStock as SStock;
use Sunnydevbox\TWInventory\Traits\StockExpirationModelTrait;

use Carbon\Carbon;

class Stock extends SStock
{
    use NodeTrait;
    use StockExpirationModelTrait;
    
    protected $hidden = [
        'updated_at',
        'created_at',
        'deleted_at',
        'user_id',

        '_lft',
        '_rgt',
    ];

    public function item()
    {
        return $this->belongsTo('Sunnydevbox\TWInventory\Models\Inventory', 'inventory_id', 'id');
    }

    public function movements()
    {
        return $this->hasMany('Sunnydevbox\TWInventory\Models\StockMovement', 'stock_id', 'id');
    }

    public function transactions()
    {
        return $this->hasMany('Sunnydevbox\TWInventory\Models\Transaction', 'stock_id', 'id');
    }

    public function location()
    {
        return $this->hasOne('Sunnydevbox\TWInventory\Models\Location', 'id', 'location_id');
    }

    public function scopeExpiredStocks($query)
    {
        $query->where([
            ['expiration_date', '<', Carbon::today()->toDateString()],
            ['quantity', '>', 0]
        ]);
    }

    public function scopeNearExpiryStocks($query)
    {
        $today = Carbon::today();
        $query->whereBetween( 'expiration_date', [ $today->toDateString(), $today->addMonths(2)->toDateString() ] )
                ->where( 'quantity', '>', 0 );
    }
}