<?php
namespace Sunnydevbox\TWInventory\Models;

use Kalnoy\Nestedset\NodeTrait;
use Sunnydevbox\TWCore\Models\BaseModel;

class PurchaseOrderItem extends BaseModel
{
    use NodeTrait;

    protected $table = 'purchase_order_items';

    protected $hidden = [
        '_lft',
        '_rgt',
        // 'created_at',
        'updated_at',
        // 'purchase_order_id',
        'user_id',
    ];

    protected $fillable = [
        'purchase_order_id',
        'inventory_transaction_id',
        'price',
        'quantity',
        'unit',
        'total',
        'notes',
        'qty_served',
        'user_id',
        'parent_id'
    ];

    public function purchaseOrder()
    {
        return $this->belongsTo('Sunnydevbox\TWInventory\Models\PurchaseOrder', 'purchase_order_id', 'id');
    }

    public function transaction()
    {
        return $this->belongsTo('Sunnydevbox\TWInventory\Models\Transaction', 'inventory_transaction_id', 'id');
    }


    protected static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->total = $model->price * $model->quantity;
        });

        self::updating(function($model){
            $model->total = $model->price * $model->quantity;
        });

        PurchaseOrderItem::observe(new \Sunnydevbox\TWInventory\Observers\PurchaseOrderItemObserver);
    }

}
