<?php
namespace Sunnydevbox\TWInventory\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

use Sunnydevbox\TWInventory\Models\StateInterface;
use Sunnydevbox\TWInventory\Models\SaleOrderProcessOrderTrait;

class SaleOrder extends BaseModel implements StateInterface
{   
    use SaleOrderProcessOrderTrait;

    
    protected $table = 'sale_orders';
    
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'code',
        'po_number',
        'status',
        'notes',

        'customer_id',
        'customer_name',
        'customer_address',
        'customer_city',
        'customer_region',
        'customer_zip',
        'customer_country',

        'billing_name',
        'billing_address',
        'billing_city',
        'billing_region',
        'billing_zip',
        'billing_country',

        'shipping_name',
        'shipping_address',
        'shipping_city',
        'shipping_region',
        'shipping_zip',
        'shipping_country',

        'delivery_date',
        'order_date',

        'supplier_id',
        'delivery_method_id',
        'payment_term_id',
        'location_id'

    ];

    public function items()
    {
        return $this->hasMany('Sunnydevbox\TWInventory\Models\SaleOrderItem', 'sale_order_id', 'id');
    }

    public function supplier()
    {
        return $this->belongsTo('Sunnydevbox\TWInventory\Models\Supplier', 'supplier_id', 'id');
    }

    public function delivery_method()
    {
        return $this->belongsTo('Sunnydevbox\TWInventory\Models\DeliveryMethod', 'delivery_method_id', 'id');
    }

    public function payment_term()
    {
        return $this->belongsTo('Sunnydevbox\TWInventory\Models\PaymentTerm', 'payment_term_id', 'id');
    }

    protected static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->status = self::STATE_SO_PARKED;
        });
    }
}