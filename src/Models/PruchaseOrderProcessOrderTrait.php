<?php
namespace Sunnydevbox\TWInventory\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

use Sunnydevbox\TWInventory\Models\PruchaseOrderProcessOrderValidateTrait;

trait PruchaseOrderProcessOrderTrait
{   
    use PruchaseOrderProcessOrderValidateTrait;

    /**
     * PO is tagged ready for costing/approval from 
     * procurement department.
     */
    public function placed()
    {
        $this->validateProcess(self::STATE_PO_PLACED);

        $this->status = self::STATE_PO_PLACED;    
        $this->save();
    }

    /**
     * Draft version
     */
    public function parked()
    {
        $this->validateProcess(self::STATE_PO_PARKED);

        $this->status = self::STATE_PO_PARKED;    
        $this->save();
    }

    /**
     * Approver has approved the PO
     * and ready to be sent to supplier
     */
    public function costed()
    {
        $this->validateProcess(self::STATE_PO_COSTED);
        $this->status = self::STATE_PO_COSTED;    
        $this->save();
    }

    /**
     * Indicates that the PO is sent
     * to supplier via email or mail (printed version)
     */
    public function receipted()
    {
        $this->status = self::STATE_PO_RECEIPTED;    
        $this->save();
    }

    /** 
     * Indicates that not all ordered items are delivered.
     */
    public function partial_served()
    {
        $this->status = self::STATE_PO_PARTIAL_SERVED;    
        $this->save();
    }

    /**
     * Indicates that the PO 
     * has completed its lifecycle
     */
    public function complete()
    {
        $this->status = self::STATE_PO_COMPLETED;    
        $this->save();
    }

    public function deleteda()
    {
        $this->status = self::STATE_PO_DELETED;    
        $this->save();
    }
}