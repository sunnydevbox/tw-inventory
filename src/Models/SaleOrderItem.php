<?php
namespace Sunnydevbox\TWInventory\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

class SaleOrderItem extends BaseModel
{
    protected $table = 'sale_order_items';

    protected $hidden = [];

    protected $fillable = [
        'inventory_transaction_id',
        'sale_order_id',
        'price',
        'quantity',
        'unit',
        'total',
        'notes',
        'box_id'
    ];


    public function transaction()
    {
        return $this->belongsTo('Sunnydevbox\TWInventory\Models\Transaction', 'inventory_transaction_id', 'id');
    }


    protected static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->total = $model->price * $model->quantity;
        });

        self::updating(function($model){
            $model->total = $model->price * $model->quantity;
        });
    }
}