<?php
namespace Sunnydevbox\TWInventory\Models;

trait HistoryTrackingTrait
{
    public function actor()
    {
        $this->belongsTo(config('auth.providers.users.model'));
    }
}