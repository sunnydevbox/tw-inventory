<?php
namespace Sunnydevbox\TWInventory\Observers;
use Sunnydevbox\TWInventory\Models\Inventory;

class InventoryObserver
{
    // creating, created, updating, updated, saving, saved, deleting, deleted,  restoring, restored

    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function created(Inventory $inventory)
    {   
        $action = 'CREATED';
        if (count($inventory->getDirty())) {
            // $this->buildData('Employee', $action, $inventory);
        }
        
        event(new \Sunnydevbox\TWInventory\Events\InventoryCreatedEvent($inventory));
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  User  $user
     * @return void
     */
    public function deleted(Inventory $inventory)
    {
        // event(new \Sunnydevbox\TWPim\Events\EmployeeDeletedEvent($inventory));
    }

    public function updated(Inventory $inventory) 
    {
        $action = 'UPDATED';
        if (count($inventory->getDirty())) {
            // $this->buildData(request('type'), $action, $inventory);
        }

        // event(new \Sunnydevbox\TWPim\Events\EmployeeUpdatedEvent($inventory));        
    }

    public function saved(Inventory $inventory) 
    {
        //echo 2;
    }

    private function buildData($type, $action, $inventory) 
    {
        $description = join(', ', collect($inventory->getDirty())->keys()->all());
        // event(new \Sunnydevbox\TWPim\Events\EmployeeLogEvent(ucfirst($type)  . ' ' . $action, $inventory, $description));
    }
}