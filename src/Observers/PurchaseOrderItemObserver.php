<?php
namespace Sunnydevbox\TWInventory\Observers;


use Sunnydevbox\TWInventory\Events\PurchaseOrderItemCreatedEvent;
use Sunnydevbox\TWInventory\Events\PurchaseOrderItemDeletedEvent;
use Sunnydevbox\TWInventory\Events\PurchaseOrderItemUpdatedEvent;
use Sunnydevbox\TWInventory\Events\PurchaseOrderItemServedEvent;

use Sunnydevbox\TWInventory\Models\PurchaseOrderItem;
use Auth;


class PurchaseOrderItemObserver
{
    // creating, created, updating, updated, saving, saved, deleting, deleted,  restoring, restored

    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function created(PurchaseOrderItem $purchaseOrderItem)
    {   
        $action = 'CREATED';
        if (count($purchaseOrderItem->getDirty())) {
            // $this->buildData('Employee', $action, $purchaseOrderItem);
        }

        event(new PurchaseOrderItemCreatedEvent($purchaseOrderItem));
        // event(new \Sunnydevbox\TWPim\Events\EmployeeCreatedEvent($purchaseOrderItem));
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  User  $user
     * @return void
     */
    public function deleted(PurchaseOrderItem $purchaseOrderItem)
    {
        // event(new \Sunnydevbox\TWPim\Events\EmployeeDeletedEvent($purchaseOrderItem));
    }

    public function updated(PurchaseOrderItem $purchaseOrderItem) 
    {
        $action = 'UPDATED';
        if (count($purchaseOrderItem->getDirty())) {
            // $this->buildData(request('type'), $action, $purchaseOrderItem);
        }

        event(new PurchaseOrderItemUpdatedEvent($purchaseOrderItem));
    }

    public function saving(PurchaseOrderItem $purchaseOrderItem) 
    {
        
        if ($user = Auth::user()) {
            $purchaseOrderItem->user_id = $user->id;
            
        }
        // dd($purchaseOrderItem, );   
    }
}