<?php

//asd
if (app()->getProvider('\Dingo\Api\Provider\LaravelServiceProvider')) {

	$api = app('Dingo\Api\Routing\Router');

	$api->version('v1', ['middleware' => ['api.auth']], function ($api) {
		
		//$api->resource('inventories', 				'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\InventoryController');
		
		
		//$api->resource('inventory/stock-movements', '\Sunnydevbox\TWInventory\Http\Controllers\API\V1\InventoryStockMovementController');
		//$api->resource('inventory/transaction-histories', 		'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\InventoryTransactionHistoryController');
		
	

		$api->group(['prefix' => 'inventory'], function($api) {
			$api->resource('products', 					'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\InventoryController');
			$api->resource('categories', 				'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\CategoryController');
			$api->resource('suppliers', 				'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\SupplierController');	
			$api->resource('metrics', 					'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\MetricController');
			$api->resource('locations', 				'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\LocationController');

			$api->post('transactions/release/{id}', 	'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\InventoryTransactionController@releaseServe');
			
			$api->resource('transactions', 				'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\InventoryTransactionController');
			$api->resource('skus', 						'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\InventorySkuController');
			$api->resource('stocks', 					'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\InventoryStockController');
			$api->resource('stock-movements', 			'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\InventoryStockMovementController');
			$api->get('suppdata', 						'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\SuppDataController@index');
			$api->resource('customers', 				'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\CustomerController');

			$api->resource('purchase-orders', 			'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\PurchaseOrderController');
			$api->resource('purchase-order-items', 		'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\PurchaseOrderItemController');
			$api->post('purchase-order-items/{action}/{id}',	'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\PurchaseOrderItemController@serve');
			$api->post('purchase-orders/{action}/{id}', '\Sunnydevbox\TWInventory\Http\Controllers\API\V1\PurchaseOrderController@processOrder');

			$api->resource('sales-orders', 				'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\SalesOrderController');
			$api->resource('sales-order-items', 		'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\SalesOrderItemController');
			$api->post('sales-order-items/{action}/{id}',	'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\SalesOrderItemController@serve');
			$api->post('sales-orders/{action}/{id}', '\Sunnydevbox\TWInventory\Http\Controllers\API\V1\SalesOrderController@processOrder');

			$api->resource('payment-terms', 			'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\PaymentTermController');
			$api->resource('delivery-methods', 			'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\DeliveryMethodController');
			$api->resource('manufacturers', 			'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\ManufacturerController');


			$api->resource('settings', 				'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\SettingController');

			/**
			 * Prepare api for inventory per branch stocks
			 */
			$api->get('notification/send-all', 			'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\InventoryThresholdController@getNotifications');
			$api->resource('thresholds', 			'\Sunnydevbox\TWInventory\Http\Controllers\API\V1\InventoryThresholdController');

		});

	});

}